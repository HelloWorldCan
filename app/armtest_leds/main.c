////////////////////////////////////////////////////////////////////////////////
//
//                 Philips LPC210X LED Example
//
// Description
// -----------
// This example demonstrates writing to the GPIO port by using four leds connected to P0.16, P0.17, P0.18, P0.19
//   (It is also a way to see if the initialization functions are correct)
//version 1.0 15/08/2005
//Author : Guillaume LAGARRIGUE
////////////////////////////////////////////////////////////////////////////////

#include <types.h>
#include <LPC210x.h>
#include "config.h"
#include "armVIC.h"

/*////////////////////////////////////////////////////////INITIALISATION FUNCTIONS///////////////////////////////////*/


/**
 *  Function Name: lowInit()
 *
 * Description:
 *    This function starts up the PLL then sets up the GPIO pins before
 *    waiting for the PLL to lock.  It finally engages the PLL and
 *    returns
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *  
 */
static void lowInit(void) {
    // set PLL multiplier & divisor.
    // values computed from config.h
    PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;

    // enable PLL
    PLLCON = PLLCON_PLLE;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup the parallel port pin
    IOCLR = PIO_ZERO_BITS;                // clear the ZEROs output
    IOSET = PIO_ONE_BITS;                 // set the ONEs output
    IODIR = PIO_OUTPUT_BITS;              // set the output bit direction

    // wait for PLL lock
    while (!(PLLSTAT & PLLSTAT_LOCK))
        continue;

    // enable & connect PLL
    PLLCON = PLLCON_PLLE | PLLCON_PLLC;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup & enable the MAM
    MAMTIM = MAMTIM_CYCLES;
    MAMCR = MAMCR_FULL;

    // set the peripheral bus speed
    // value computed from config.h
    VPBDIV = VPBDIV_VALUE;                // set the peripheral bus clock speed
}


/**
 *  Function Name: sysInit()
 *
 * Description:
 *    This function is responsible for initializing the program
 *    specific hardware
 *
 * Calling Sequence: 
 *    void
 *
 * Returns:
 *    void
 *  
 */
static void sysInit(void) {
    lowInit();                            // setup clocks and processor port pins

    // set the interrupt controller defaults
    #define RAM_RUN
    #if defined(RAM_RUN)

    MEMMAP = MEMMAP_SRAM;                 // map interrupt vectors space into SRAM
    #elif defined(ROM_RUN)

    MEMMAP = MEMMAP_FLASH;                // map interrupt vectors space into FLASH
    #else
    #error RUN_MODE not defined!
    #endif

    VICIntEnClear = 0xFFFFFFFF;           // clear all interrupts
    VICIntSelect = 0x00000000;            // clear all FIQ selections
    VICDefVectAddr = (uint32_t)reset;     // point unvectored IRQs to reset()

    //  wdtInit();                            // initialize the watchdog timer
    //  initSysTime();                        // initialize the system timer
    //  uart0Init(UART_BAUD(HOST_BAUD), UART_8N1, UART_FIFO_8); // setup the UART
}



/*////////////////////////////LEDS INITIALISATION///////////////////////*/

#define NUM_LEDS 4
static int leds[] = { 0x10000, 0x20000, 0x40000, 0x80000 };
static int All_leds = 0xF0000;


/**
 * Initializes leds.
 */
static void
ledInit() {
    IODIR |= 0x000F0000; /*leds connected to P0.16 17 18 & 19 should blink*/
    IOSET = 0x000F0000;   /* all leds are switched on */
}

/**
 * Switches the leds off.
 * @param led  switched off led number. (integer)
 */
static void   /* Ioclr.i =1   =>   IOset.i cleared */
ledOff(int led) {
    IOCLR = led;
}

/**
 * Switches the leds on. 
 * @param led  switched on led number. (integer)
 */
static void    /*  Ioset.i = 1   =>  P0.i = 1    */
ledOn(int led) {
    IOSET = led;
}

/**
 * Creates a delay
 * @param d duration (unit not defined yet)
 */
void
delay(int d) {
    volatile int x;
    int i;
    for (i = 0; i < 10; i++)
        for(x = d; x; --x)
            ;
}



/*/////////////////////////////////////MAIN///////////////////////////////*/

/**
 * Makes the leds blink four times together then two times one after one
 *   
 * 
 */
int
main(void) {

    sysInit();

    int i;
    MAMCR = 2;
    ledInit();
    while (1) {
        for (i=0; i < 4 ; i++) {
            ledOn(All_leds);
            delay ( 20000 );
            ledOff (All_leds);
        }

        for (i = 0; i < NUM_LEDS; ++i) {
            ledOn(leds[i]);
            delay(20000);
        }
        for (i = 0; i < NUM_LEDS; ++i) {
            ledOff(leds[i]);
            delay(20000);
        }
        for (i = NUM_LEDS - 1; i >= 0; --i) {
            ledOn(leds[i]);
            delay(20000);
        }
        for (i = NUM_LEDS - 1; i >= 0; --i) {
            ledOff(leds[i]);
            delay(20000);
        }

    }
    return 0;
}


