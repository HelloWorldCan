/********************************************************/
/*		Hello World for LPCAN_VCA		*/
/*							*/
/* This software recieves messages via CAN bus. If	*/
/* message with right data (0x1) is recieve, adc value  */
/* is send also via can.				*/
/* This software is an example how to use lpcan.h and	*/
/* lpcan_vca.h.						*/
/*							*/
/* Autor: Martin Rakovec (martin.rakovec@tiscali.cz)	*/
/********************************************************/

/* LPC21xx definitions */
#include <lpc21xx.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>

/* can handle*/
vca_handle_t can;
/*own can message*/
canmsg_t msg = {.flags = 0};

/** ADC initialization 
* This function initialize AD convertor
*
* @param clk_div clock div
*
*/
void adc_init(unsigned clk_div) {
	/* Set AD control registry */
	ADCR = ADC_CR_ADC0_m | ((clk_div&0xff)<<8)/*ADC clock*/ | ADC_CR_PDN_ON_m;  
}

/**
 * Main function 
 */
int main (void)  {

	VPBDIV = 1;
	
	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0x00000000;	

	/* CAN bus setup */
	uint32_t btr;
	lpcan_btr(&btr, 1000000 /*Bd*/, 10000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
	lpc_vca_open_handle(&can, 0/*device*/, 0/*flags*/, btr, 10, 11, 12);	

	/*init adc*/
	adc_init(3);

	while(1){
		/* if nothig is recieved, continue*/
		if(vca_rec_msg_seq(can,&msg,1)<0) continue;
		switch (msg.data[0]){
		/* if message with id = 1 send adc value*/ 
		case 0x0:
			ADCR = ADC_CR_ADC0_m | ((14&0xff)<<8)/*ADC clock*/ | ADC_CR_PDN_ON_m; 
			ADCR |= (1<<24); /* star conversion now */
			/* wait for end of conversion */
			while((ADDR & (1<<31)) == 0);
			/*set can message*/	
			msg.id = 0x0;
			msg.length = 1;
			/* Read value from AIN0*/
			msg.data[0] = ((ADDR>>6)&0x3ff);
		    	vca_send_msg_seq(can,&msg,1);
		    	break;
		case 0x1:
			ADCR = ADC_CR_ADC1_m | ((14&0xff)<<8)/*ADC clock*/ | ADC_CR_PDN_ON_m; 
			ADCR |= (1<<24); /* star conversion now */
			/* wait for end of conversion */
			while((ADDR & (1<<31)) == 0);
			/*set can message*/	
			msg.id = 0x1;
			msg.length = 1;
			/* Read value from AIN0*/
			msg.data[0] = ((ADDR>>6)&0x3ff);
		    	vca_send_msg_seq(can,&msg,1);
		    	break;
		default: 
			/*send ee*/
		    	msg.id = 0xe;
		    	msg.length = 1;
		    	msg.data[0] = 0xee;
		    	vca_send_msg_seq(can,&msg,1);
		    	//for(i = 0;i<1000000;i++);
		    	break;	
		}
	}
}



