/* CAN loader */
#include <string.h>
#include <periph/can.h>

#define CANLOAD_HEAD 0x7fe
#define CANLOAD_BODY 0x7ff

#define CANLOAD_CMD_NOP   0x00
#define CANLOAD_CMD_LOAD  0x01
#define CANLOAD_CMD_GO    0x02
#define CANLOAD_CMD_ON    0xfa

#ifndef CANLOAD_ID
#warning Unique 32bit number CANLOAD_ID must be defined for each compiled target board.
#define CANLOAD_ID 0
#endif
extern uint32_t canload_id;

#define STATUS_LED
#ifdef STATUS_LED
/* define i/o port addr&values for lighting status LED */
#define LED_PINSEL2     PINSEL2
#define LED_PINSEL_VAL2 0xfffffff3
#define LED_IODIR2      IODIR1
#define LED_IOSET2      IOSET1
#define LED_IOCLR2      IOCLR1
#define LED_IOBIT3      0x00080000

#define LED_PINSEL     PINSEL1
#define LED_PINSEL_VAL 0xfffc0fff;
#define LED_IODIR      IODIR0
#define LED_IOSET      IOSET0
#define LED_IOCLR      IOCLR0
#define LED_IOBIT0     0x00400000
#define LED_IOBIT1     0x00800000
#define LED_IOBIT2     0x01000000
#endif

can_msg_t received_msg;

void rx_msg(uint32_t id) {
  do {
    while (!can_msg_received);
    can_msg_received = 0;
  } while (can_rx_msg.id != id); 
  memcpy(&received_msg, (void*)&can_rx_msg, sizeof(can_msg_t));
}

int main() {
  can_msg_t msg = {.flags = 0, .dlc = 5, .id = CANLOAD_ID & 0x7ff};
  uint8_t cmd;
  void *address;
  void (*user_code)();

  canload_id = CANLOAD_ID;
  /* peripheral clock = CPU clock (10MHz) */
  VPBDIV = 1;
  /** map exception handling to ROM **/
  MEMMAP = 0x1;
  /* init Vector Interrupt Controller */
  VICIntEnClr = 0xFFFFFFFF;
  VICIntSelect = 0x00000000;
#ifdef STATUS_LED
  /* turn LED on */
  LED_PINSEL &= LED_PINSEL_VAL;
  LED_PINSEL2 &= LED_PINSEL_VAL2;
  LED_IODIR |= LED_IOBIT0;
  LED_IODIR |= LED_IOBIT1;
  LED_IODIR |= LED_IOBIT2;
  LED_IODIR2 |= LED_IOBIT3;
  LED_IOCLR = LED_IOBIT0;
  LED_IOSET = LED_IOBIT1;
  LED_IOCLR = LED_IOBIT2;
  LED_IOCLR2 = LED_IOBIT3;
#endif
  /* 10MHz CPU, 10MHz VPB, 1Mb/s CAN */
  can_init(0x00250000, 14, NULL);
  /* announce ourself to the world */
  msg.data[4] = CANLOAD_CMD_ON;
  ((uint32_t*)msg.data)[0] = canload_id;
#ifdef STATUS_LED
  LED_IOCLR = LED_IOBIT1;
#endif
  while (can_tx_msg(&msg));
#ifdef STATUS_LED
  LED_IOSET = LED_IOBIT1;
#endif
  /* command processing loop */
  for (;;) {
    /* wait for loader HEAD message */
    rx_msg(CANLOAD_HEAD);
    /* check target ID and mask */
    if ((received_msg.dlc != 8) ||
	(((uint32_t*)received_msg.data)[0] !=
	 (((uint32_t*)received_msg.data)[1] & canload_id)))
      continue;
#ifdef STATUS_LED
    LED_IOCLR = LED_IOBIT1;
#endif
    /* receive loader command message */
    rx_msg(CANLOAD_BODY);
    switch (cmd = received_msg.data[0]) {
    case CANLOAD_CMD_NOP:
      break;
    case CANLOAD_CMD_LOAD:
      /* set starting load address */
      memcpy(&address, (void*)received_msg.data + 1, 4);
      /* receive data, until last packet (~shorter msg) arrive */
      do {
	rx_msg(CANLOAD_BODY);
	memcpy(address, (void*)received_msg.data, received_msg.dlc);
	address += received_msg.dlc;
      } while (received_msg.dlc == 8);
      break;
    case CANLOAD_CMD_GO:
      memcpy(&user_code, (void*)received_msg.data + 1, 4);
      /* GO! */
      user_code();
      break;
    default:
      /* unrecognized command -- do not acknowledge */
      continue;
    }
#ifdef STATUS_LED
    LED_IOSET = LED_IOBIT1;
#endif
    /* command executed -- acknowledge */
    msg.data[4] = cmd;
    ((uint32_t*)msg.data)[0] = canload_id;
    while (can_tx_msg(&msg));
  }
}

/* KOHE||, */
