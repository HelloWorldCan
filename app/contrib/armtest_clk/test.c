
/*
 * This is a little test application to test your eclipse IDE with the
 * GNUARM-Toolchain and newlib-lpc_rel_2 provided by aeolus
 */

#include <stdio.h>
#include <errno.h>

#include <lpc210x.h>
#include <dev_cntrl.h>
#include <lpc_ioctl.h>
#include <lpc_sys.h>





int main(void)
{
unsigned long actual_freq;	

	/*
	 * set oscillator frequency (round it up ;)
	 * 7373kHz (7,373MHz)
	 */
	SetNativeSpeed(_impure_ptr, 7373uL);
	
	/*
	 * set up memory access, CPU and bus speeds, this may be very oversized,
	 * just copied and pasted from the newlib examples...
	 */
	
	SetMAM(_impure_ptr, 3u, MAM_full_enable);
	VPBControl(_impure_ptr, VPB_DIV1);
	SetDesiredSpeed(_impure_ptr, 60000uL);
	
	
	StartClock(_impure_ptr);
	
	actual_freq = ActualSpeed();
	
	return(0);
}
