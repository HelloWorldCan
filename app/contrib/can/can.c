#include "can.h"

volatile int can_msg_received = 0;
volatile can_msg_t can_rx_msg;

/* private global variables */
uint32_t *can_rx_msg_data = (uint32_t*)can_rx_msg.data;
can_rx_callback can_rx_cb;

void can_init(uint32_t btr, unsigned rx_isr_vect, can_rx_callback rx_cb) {
  /* enable CAN1 Rx pin */
  PINSEL1 |= 0x00040000;
  PINSEL1 &= 0xfff7ffff;
  /* receive all messages, no filtering */
  AFMR = 0x2;
  /* reset mode */
  C1MOD = 0x1;
  /* -- addition from lpc2000 maillist msg #3052: */
  C1CMR = 0x0e; //Clear receive buffer, data overrun, abort tx
  /* -- end of addition */
  C1IER = 0x0;
  C1GSR = 0x0;
  /* set baudrate & timing */
  C1BTR = btr;
  /* register Rx handler */
  can_rx_cb = rx_cb;
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (uint32_t)can_rx_isr;
  ((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x20 | 26;
  /* enable Rx int */
  VICIntEnable = 0x04000000;
  C1IER = 0x1;
  /* normal (operating) mode */
  C1MOD = 0x0;
#if 0
  /* LPC2119 CAN.5 erratum workaround */
  C1TFI1 = 0x00000000;
  C1TID1 = 0x0;
  C1CMR = 0x23;
#endif
}

void can_rx_isr() {
  can_rx_msg.flags = C1RFS;
  can_rx_msg.dlc = (can_rx_msg.flags>>16) & 0xf;
  can_rx_msg.id = C1RID;
  can_rx_msg_data[0] = C1RDA;
  can_rx_msg_data[1] = C1RDB;
  can_msg_received = 1;
  if (can_rx_cb != NULL)
    can_rx_cb((can_msg_t*)&can_rx_msg);
  /* release Rx buffer */
  C1CMR = 0x4;
  /* int acknowledge */
  VICVectAddr = 0;
}

int can_tx_msg(can_msg_t *tx_msg) {
  uint32_t *data = (uint32_t*)tx_msg->data;

  /* check, if buffer is ready (previous Tx completed) */
  if ((C1SR & 0x4) == 0)
    return -1; /* busy */
  C1TFI1 = (tx_msg->flags & 0xc0000000) |
    ((tx_msg->dlc<<16) & 0x000f0000);
  C1TID1 = tx_msg->id;
  C1TDA1 = data[0];
  C1TDB1 = data[1];
  /* start transmission */
  C1CMR = 0x21;
  return 0; /* OK */
}

/*EOF*/
