#include <lpc21xx.h>
#include <types.h>


void delay() {
  unsigned u;

  for (u = 0; u < 1000000; u++);
}

/*** PWM ***/

int PWM_PINSEL[] = {
  /*nothing*/ 1, /*PWM1*/ 1, 15, 3, 17, /*PWM5*/ 11, /*PWM6*/ 19
};

uint32_t *PWM_MR[] = {
  (uint32_t*)&(PWMMR0),
  (uint32_t*)&(PWMMR1),
  (uint32_t*)&(PWMMR2),
  (uint32_t*)&(PWMMR3),
  (uint32_t*)&(PWMMR4),
  (uint32_t*)&(PWMMR5),
  (uint32_t*)&(PWMMR6)
};

void pwm_channel(int n, int double_edge) {
  uint32_t bit;

  PWMPCR |= (0x100 | (double_edge && n)) << n;
  if (n == 5) {
    PINSEL1 |= 0x00000400;
    PINSEL1 &= 0xfffff7ff;
  }
  else {
    bit = 1 << PWM_PINSEL[n];
    PINSEL0 |= bit;
    bit = ~(bit >> 1);
    PINSEL0 &= bit;
  }
}

void pwm_set(int n, uint32_t when) {
  *PWM_MR[n] = when;
  PWMLER |= 1 << n;
}

void pwm_set_double(int n, uint32_t from, uint32_t to) {
  *PWM_MR[n-1] = from;
  *PWM_MR[n] = to;
  PWMLER |= 0x3 << (n-1);
}

void pwm_init(uint32_t prescale, uint32_t period) {
  PWMPR = prescale;
  PWMMR0 = period;
  PWMLER |= 0x1;
  PWMMCR |= 0x00000002;
  PWMTCR &= ~0x2;
  PWMTCR |= 0x9;
}

/***********/

void motor_drive(float u) {
  uint32_t d = (float)PWMMR0*(0.5*(1.0+u));

  pwm_set_double(2, 0, d);
  pwm_set_double(4, d, 0);
}


int main() {
  pwm_channel(2, 1);
  pwm_channel(4, 1);
  pwm_init(0, 50);

  motor_drive(0);

  for (;;);
}
