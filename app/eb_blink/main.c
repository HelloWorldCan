////////////////////////////////////////////////////////////////////////////////
//
//                 Eurobot BLINK TEST  (with LPC2129)
//
// Description
// -----------
// This software blinks on debug leds. Use it with lpceurobot board
// Author : Jiri Kubias DCE CVUT
//
//
////////////////////////////////////////////////////////////////////////////////

#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <deb_led.h>
		

void dummy_wait()
{
	unsigned int wait = 5000000;
	while(--wait);
}

int main (void)  {


	

	while(1)
	{	
		deb_led_change(LEDR);
		dummy_wait();	
		deb_led_change(LEDG);
		dummy_wait();
		deb_led_change(LEDB);
		dummy_wait();
		deb_led_change(LEDY);
		dummy_wait();

	} 
}



