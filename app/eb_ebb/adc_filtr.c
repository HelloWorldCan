#include <lpc21xx.h>                          // LPC21XX Peripheral Registers
#include <types.h> 
#include "adc_filtr.h"
#include <stdlib.h>
#include <string.h>
#include <deb_led.h>



#define ADCCH0 22	///< ADC0 value for PINSEL
#define ADCCH1 24	///< ADC1 value for PINSEL
#define ADCCH2 26	///< ADC2 value for PINSEL
#define ADCCH3 28	///< ADC3 value for PINSEL


/**
 *  Comparsion function  for quicksort
 *
 * @param *a	first number for comparion
 * @param *b	second number for comparion
 *
 * @return 	1 if b is higher than a
 */ 
int compare( const void *a, const void *b)
{
	return (*((unsigned int*)a) < *(((unsigned int*)b)));
}


/**
 *  Median filter for ADC. If new data is available the median is recalculated. 
 *  This function may be called from main function, it should take long time
 *  to calculate the median for all ADC value.
 * 
 * @param chan	bit oriented (0~3) value, defines which ADC channels have new data
 */ 
void adc_filter(unsigned char chan)
{
	if(adc_update_adc & 1) 
	{
		memcpy(&adc_filtr_0_m, adc_filtr_0, sizeof(uint16_t) * ADC_FILTR_SIZE );
		qsort(adc_filtr_0_m , ADC_FILTR_SIZE , sizeof(uint16_t), compare);
		adc_val[0] = adc_filtr_0_m[(ADC_FILTR_SIZE/2 + 1)];
		adc_update_adc &= ~ 1;

	}
	if(adc_update_adc & 2) 
	{
		memcpy(&adc_filtr_1_m, adc_filtr_1, sizeof(uint16_t) * ADC_FILTR_SIZE );
		qsort(adc_filtr_1_m , ADC_FILTR_SIZE , sizeof(uint16_t), compare);
		adc_val[1] = adc_filtr_1_m[(ADC_FILTR_SIZE/2 + 1)];
		adc_update_adc &= ~ 2;
	}
	if(adc_update_adc & 4) 
	{	
		memcpy(&adc_filtr_2_m, adc_filtr_2, sizeof(uint16_t) * ADC_FILTR_SIZE );
		qsort(adc_filtr_2_m , ADC_FILTR_SIZE , sizeof(uint16_t), compare);
		adc_val[2] = adc_filtr_2_m[(ADC_FILTR_SIZE/2 + 1)];
		adc_update_adc &= ~ 4;
	}
	if(adc_update_adc & 8) 
	{
		memcpy(&adc_filtr_3_m, adc_filtr_3, sizeof(uint16_t) * ADC_FILTR_SIZE );
		qsort(adc_filtr_3_m , ADC_FILTR_SIZE , sizeof(uint16_t), compare);
		adc_val[3] = adc_filtr_3_m[(ADC_FILTR_SIZE/2 + 1)];
		adc_update_adc &= ~ 8;
	}
}




/**
 *  ADC ISR routine. This routine reads selected ADC value and
 *  multiplies it by #ADC_MUL and adds #ADC_OFFSET to calculate the
 *  volage (3.25mV/div).  After this reading the next ADC channel is
 *  set up for measuring.
 */ 
void adc_isr_filtr(void) __attribute__ ((interrupt));
void adc_isr_filtr(void) 
{
	unsigned char chan =0;
	unsigned int val =0;


	chan = (char) ((ADDR>>24) & 0x07);
	val = (((((ADDR >> 6) & 0x3FF) * ADC_MUL + ADC_OFFSET) + adc_val[chan]) >> 1) ;


	if(chan == 0) 
	{
		adc_filtr_0[adc_filtr_p] =  val;
		adc_update_adc |= 1;
	}
	else if(chan == 1) 
	{
		adc_filtr_1[adc_filtr_p] =  val;
		adc_update_adc |= 2;
	}
	else if(chan == 2) 
	{	
		adc_filtr_2[adc_filtr_p] =  val;
		adc_update_adc |= 4;
	}
	else if(chan == 3) 
	{
		adc_filtr_3[adc_filtr_p] =  val;
		adc_update_adc |= 8;
		
		if(adc_filtr_p == (ADC_FILTR_SIZE -1 )) adc_filtr_p = 0;
		else ++adc_filtr_p;
	}


	ADCR &= ~(ADC_CR_START_OFF_m);


	switch(chan)
	{
		case 0:
			ADCR = ((ADC_CR_ADC1_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
			break;

		case 1:
			ADCR = ((ADC_CR_ADC2_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
			break;
			
		case 2:
			ADCR = ((ADC_CR_ADC3_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
			break;
			
		case 3:
			ADCR = ((ADC_CR_ADC0_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
			break;															 
	}
	
	 VICVectAddr = 0;

}

/**
 *  Inicializes ADC service for all ADC (4) channels and installs ISR routine to VIC.
 *   Automaticly starts the conversion of first channel on given conversion frequency.
 */ 
void init_adc_filter(unsigned rx_isr_vect)
{
	
	PINSEL1 |= ((PINSEL_1 << ADCCH0) | (PINSEL_1 << ADCCH1) | (PINSEL_1 << ADCCH2) | (PINSEL_1 << ADCCH3));		

	int x;

	for (x = 0; x < 21; ++x)
	{
		adc_filtr_0[x] = 0;
		adc_filtr_1[x] = 0;
		adc_filtr_2[x] = 0;
		adc_filtr_3[x] = 0;
	}

	adc_filtr_p = 0;
	adc_update_adc = 0;

	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x32;
	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned) adc_isr_filtr;
	VICIntEnable = 0x40000;

	ADCR = ((ADC_CR_ADC0_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
}
