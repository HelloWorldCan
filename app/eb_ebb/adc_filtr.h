#ifndef ADC_FILTR_H
#define ADC_FILTR_H

#include <system_def.h>


#define ADC_MUL 	1	///< This value multiplies mesured value from ADC
#define ADC_OFFSET	0	///< This valueis added to mesured value from ADC

#define ACD_HZ		3000	///< Sampling frequency

#define ADC_VPB_DIV 	0x80	///< VPB divisor



#if ADC_VPB_DIV & 0xffffff00
	#error Wrong ACD_HZ
#endif


#define ADC_FILTR_SIZE	201	///< Size of the median filter for each channel, be aware sinal size is size * 8

volatile unsigned int adc_val[4];	///< Final ADC value
volatile unsigned int adc_update_adc;	///< bite oriented, updated when new ADC channel is measured



uint16_t adc_filtr_0_m[ADC_FILTR_SIZE];		///< 
uint16_t adc_filtr_1_m[ADC_FILTR_SIZE];
uint16_t adc_filtr_2_m[ADC_FILTR_SIZE];
uint16_t adc_filtr_3_m[ADC_FILTR_SIZE];



volatile uint16_t adc_filtr_0[ADC_FILTR_SIZE];
volatile uint16_t adc_filtr_1[ADC_FILTR_SIZE];
volatile uint16_t adc_filtr_2[ADC_FILTR_SIZE];
volatile uint16_t adc_filtr_3[ADC_FILTR_SIZE];
volatile uint16_t adc_filtr_p ;

void adc_filter(unsigned char chan);



void init_adc_filter(unsigned rx_isr_vect);

#endif
