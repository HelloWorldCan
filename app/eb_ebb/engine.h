/**
 * @file   engine.h
 * 
 * @brief  Engines control
 * 	This file provides simply how-to use eb_ebb library.
 * 	From main function is called init_perip function 
 * 	where is initialized servos, engines, power switch,
 * 	CAN,ADC and serial port. After this initialization is shown 
 *  how to control each devices. This sample also include simply 
 *  sample of sending and receiving CAN message.
 * 
 *  Due to small number of PWM channels are only one PWM assigned 
 *  to one motor. For this case there is two ways how to stop the 
 *  engine. If you use engine_x_pwm(0); the motors will be stopped
 *  by shorting its coils. This will stop as fast as possible, but
 *  it also generates huge electromagnetic noise. If you want to 
 *  stop smoothly use engine_x_en(ENGINE_EN_OFF); this will disconnect
 *  the power from the engines.
 * 
 */



// Author: Jirka Kubias <jirka@Jirka-NB>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//





#ifndef ENGINE_H
#define ENGINE_H


#define ENGINE_EN_ON	1	///< Enables Engine
#define ENGINE_EN_OFF	0	///< Disables Engine


#define ENGINE_DIR_BW	1	///< Backward direction
#define ENGINE_DIR_FW	0	///< Forward direction


void init_engine_A();
void init_engine_B();

void engine_A_en(unsigned status);
void engine_B_en(unsigned status);

void engine_A_dir(unsigned dir);
void engine_B_dir(unsigned dir);

void engine_A_pwm(unsigned pwm);	// pwm is in percent, range 0~100
void engine_B_pwm(unsigned pwm);	// pwm is in percent, range 0~100



#endif
