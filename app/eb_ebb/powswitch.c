#include <lpc21xx.h>                            /* LPC21xx definitions */
#include "powswitch.h"





#define ESW (1<<16)	///< Pin define



void init_pow_switch(void)
{
	IO0DIR |= ESW;			// enable output
	IO0CLR |= ESW;			// sets to LOW level 


	PINSEL1 &= ~(PINSEL_3 << 0);	// set as GPIO
}

void pow_switch_on(void)
{
	IO0SET |= ESW;			// sets to LOW level 
}


void pow_switch_off(void)
{
	IO0CLR |= ESW;			// sets to LOW level 
}
