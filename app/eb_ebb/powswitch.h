#ifndef POWSWITCH_H
#define POWSWITCH_H

/** Initializes power switch
 */
void init_pow_switch(void);


/** Power switch ON
 */
void pow_switch_on(void);

/** Power switch OFF
 */
void pow_switch_off(void);

#endif
