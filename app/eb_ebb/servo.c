

#include <lpc21xx.h>                          // LPC21XX Peripheral Registers
#include <deb_led.h>
#include <system_def.h>
#include "servo.h"


#define SERVO2 (1<<10)
#define SERVO0 (1<<12)
#define SERVO1 (1<<13)



#define TIM_EMR_NOTHING 0
#define TIM_EMR_CLEAR 	1
#define TIM_EMR_SET 	2
#define TIM_EMR_TOGLE 	3

#define TIM_EMR_PIN_ON 1
#define TIM_EMR_PIN_OFF 0

#define TIME20MS 	((CPU_APB_HZ) / 50)
#define SERVOTICK	(((CPU_APB_HZ / 50) / 20) / 256)


unsigned char servo[3];



// ---------------- SERVO PART -------------------------------


void tc1 (void) __attribute__ ((interrupt));

void tc1 (void)   {
	
	time_ms +=20;
	
	T1EMR |= (TIM_EMR_PIN_ON<<0) | (TIM_EMR_PIN_ON<<1) | (TIM_EMR_PIN_ON<<3);

	T1MR0 = (servo[0] + 256) * SERVOTICK;	
	T1MR1 = (servo[1] + 256) * SERVOTICK;
	T1MR3 = (servo[2] + 256) * SERVOTICK;

	if (T1IR != 4)
	{			
	    __deb_led_on(LEDR);	
	}  

	T1IR        = 4;                            // Vynulovani priznaku preruseni
	VICVectAddr = 0;                            // Potvrzeni o obsluze preruseni
}

void set_servo(char serv, char position)
{
	if (serv == 0) servo[0] = position;
	if (serv == 1) servo[1] = position;
	if (serv == 2) servo[2] = position;
}

/* Setup the Timer Counter 1 Interrupt */
void init_servo (unsigned rx_isr_vect)
{

	IO0DIR |= (SERVO0 | SERVO1 | SERVO2);			// enables servo output
	IO0SET |= (SERVO0 | SERVO1 | SERVO2);			// sets to High level 

	PINSEL0 &= ~((PINSEL_3 << 24)	| (PINSEL_3 << 26));
	PINSEL0 |= (PINSEL_2 << 24) | (PINSEL_2 << 26);
	PINSEL1 &= ~(PINSEL_3 << 8);
	PINSEL1 |= (PINSEL_1 << 8);
	

	servo[0] = 0; 
	servo[1] = 127;
	servo[2] = 0xFF;

	T1PR = 0;
	T1MR2 = TIME20MS;
  	T1MR0 = (servo[0] + 256) * SERVOTICK;	
	T1MR1 = (servo[1] + 256) * SERVOTICK;
	T1MR3 = (servo[2] + 256)* SERVOTICK;
  	T1MCR = (3<<6);			// interrupt on MR1

	T1EMR = (TIM_EMR_PIN_ON<<0) | (TIM_EMR_PIN_ON<<1) | (TIM_EMR_PIN_ON<<3) \
				| (TIM_EMR_CLEAR << 4) | (TIM_EMR_CLEAR << 6) | (TIM_EMR_CLEAR << 10);


  	T1TCR = 1;                                  // Starts Timer 1 


  	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned long)tc1;          // Nastaveni adresy vektotu preruseni
  	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x20 | 0x5;                    // vyber casovece pro preruseni
  	VICIntEnable = (1<<5);                  // Povoli obsluhu preruseni
}


// ---------------- powSitch PART -------------------------------

