#include <lpc21xx.h>                          // LPC21XX Peripheral Registers
#include <types.h> 
#include <deb_led.h>
#include <system_def.h>
#include "pwrstep.h"


#define PWR33	(1<<22) 
#define PWR50	(1<<24)
#define PWR80	(1<<23)

#define ADC0	(1<<27) 
#define ADC1	(1<<28) 
#define ADC2	(1<<29) 
#define ADC3	(1<<30) 



#define ADCCH0 22
#define ADCCH1 24
#define ADCCH2 26
#define ADCCH3 28

#define TIME1MS 	((CPU_APB_HZ) / 1000)

//  tohla me byt definovano nekde jinde  FIXME


#define ADC_PIN_0   0x1
#define ADC_PIN_1   0x2
#define ADC_PIN_2   0x4
#define ADC_PIN_3   0x8

#define ADC_CR_ADC0 0x1
#define ADC_CR_ADC1 0x2
#define ADC_CR_ADC2 0x4
#define ADC_CR_ADC3 0x8

#define ADC_CR_CLK_DIV_1 	(1<<8)  // this nuber should be multipied  sampe
								// requested divisor 4  ---- clk_div = 4 * ADC_CR_CLK_DIV_1
#define ADC_CR_BURST     	(1<<16)
#define ADC_CR_CLKS_11     	(0<<17)
#define ADC_CR_CLKS_10     	(1<<17)
#define ADC_CR_CLKS_9     	(2<<17)
#define ADC_CR_CLKS_8     	(3<<17)
#define ADC_CR_CLKS_7     	(4<<17)
#define ADC_CR_CLKS_6     	(5<<17)
#define ADC_CR_CLKS_5     	(6<<17)
#define ADC_CR_CLKS_4     	(7<<17)

#define ADC_CR_PDN_ON     	(1<<21)

#define ADC_CR_START_OFF	(0<<24)
#define ADC_CR_START_NOW	(1<<24)
#define ADC_CR_START_P016	(2<<24)
#define ADC_CR_START_P022	(3<<24)
#define ADC_CR_START_MAT01	(4<<24)
#define ADC_CR_START_MAT03	(5<<24)
#define ADC_CR_START_MAT10	(6<<24)
#define ADC_CR_START_MAT11	(7<<24)

#define ADC_CR_EDGE_RISING	(0<<27)
#define ADC_CR_EDGE_FALLING	(1<<27)





	

void pwr_33(char mode)	// switch on/off 3,3V power line
{
	if (mode != PWR_ON)
	{
		IO1SET |= PWR33; 	
	}
	else
	{
		IO1CLR |= PWR33; 	
	}
}


void pwr_50(char mode)	// switch on/off 5V power line
{
	if (mode != PWR_ON)
	{
		IO1SET |= PWR50; 	
	}
	else
	{
		IO1CLR |= PWR50; 	
	}
}


void pwr_80(char mode)	// switch on/off 8V power line
{
	if (mode != PWR_ON)
	{
		IO1SET |= PWR80; 	
	}
	else
	{
		IO1CLR |= PWR80; 	
	}
}


 volatile char test =0xF;

void adc_isr(void) __attribute__ ((interrupt));

void adc_isr(void) 
{
	unsigned char chan =0;										   
	unsigned int val =0;


	chan = (char) ((ADDR>>24) & 0x07);
	val = ((ADDR >> 6) & 0x3FF); 

	


	adc_val[chan] = (((val * ADC_CON_CONST + ADC_OFFSET) + adc_val[chan]) >> 1) ;

	ADCR &= ~(ADC_CR_START_OFF);


	switch(chan)
	{
		case 0:
			ADCR = ((ADC_CR_ADC1) | (ADC_CR_CLKS_11) | (ADC_CR_PDN_ON) | (ADC_CR_START_NOW) | (20*ADC_CR_CLK_DIV_1));
			break;

		case 1:
			ADCR = ((ADC_CR_ADC2) | (ADC_CR_CLKS_11) | (ADC_CR_PDN_ON) | (ADC_CR_START_NOW) | (20*ADC_CR_CLK_DIV_1));
			break;
			
		case 2:
			ADCR = ((ADC_CR_ADC3) | (ADC_CR_CLKS_11) | (ADC_CR_PDN_ON) | (ADC_CR_START_NOW) | (20*ADC_CR_CLK_DIV_1));
			break;
			
		case 3:
			ADCR = ((ADC_CR_ADC0) | (ADC_CR_CLKS_11) | (ADC_CR_PDN_ON) | (ADC_CR_START_NOW) | (20*ADC_CR_CLK_DIV_1));
			break;															 
	}
	
	 VICVectAddr = 0;

}


void init_adc(unsigned rx_isr_vect)
{
	
	PINSEL1 |= ((PINSEL_1 << ADCCH0) | (PINSEL_1 << ADCCH1) | (PINSEL_1 << ADCCH2) | (PINSEL_1 << ADCCH3));		

	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x32;
	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned) adc_isr;
	VICIntEnable = 0x40000;





	ADCR = ((ADC_CR_ADC0) | (ADC_CR_CLKS_11) | (ADC_CR_PDN_ON) | (ADC_CR_START_NOW) | (10*ADC_CR_CLK_DIV_1));
}


void init_pwr(void)   // init power lines
{
	IO1DIR |= (PWR33 | PWR50 | PWR80);
	pwr_33(PWR_OFF);
	pwr_50(PWR_OFF);
	pwr_80(PWR_OFF);

	//init_adc();

}




void tc1 (void) __attribute__ ((interrupt));

void tc1 (void)   {
	
	time_ms +=1;
	
	

	if (T1IR != 4)
	{			
	    __deb_led_on(LEDR);	
	}  

	T1IR        = 4;                            // Vynulovani priznaku preruseni
	VICVectAddr = 0;                            // Potvrzeni o obsluze preruseni
}


/* Setup the Timer Counter 1 Interrupt */
void init_time (unsigned rx_isr_vect)
{

	



	T1PR = 0;
	T1MR2 = TIME1MS;
  	T1MCR = (3<<6);			// interrupt on MR1

  	T1TCR = 1;                                  // Starts Timer 1 

  	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned long)tc1;          // Nastaveni adresy vektotu preruseni
  	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x20 | 0x5;                    // vyber casovece pro preruseni
  	VICIntEnable = (1<<5);                  // Povoli obsluhu preruseni
}






