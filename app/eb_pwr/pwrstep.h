#ifndef PWRSTEP_H
#define PWRSTEP_H

#define PWR_ON	1
#define PWR_OFF	0

#define ADC_DIV 10
#define ADC_CON_CONST 322
#define ADC_OFFSET	2000


	volatile unsigned int adc_val[4];

/**	pwr_33 Switch on/off 3,3V power line
 *  @param mode  -  PWR_ON/PWR_OFF 
 */
void pwr_33(char mode);

/**	pwr_50 Switch on/off 5,0V power line
 *  @param mode  -  PWR_ON/PWR_OFF 
 */
void pwr_50(char mode);

/**	pwr_80 Switch on/off 8,0V power line
 *  @param mode  -  PWR_ON/PWR_OFF 
 */
void pwr_80(char mode);

/**	init_pwr  inicializes power lines - default: all lines is off
 */
void init_pwr(void);


/**	inicializes ADC lines and starts converion (use ISR)
 *  @param rx_isr_vect  ISR vector
 */
void init_adc(unsigned rx_isr_vect);

/** gobal time
 *  @note incremented twenty 20ms, overrun every 1194hours
 */
volatile unsigned int time_ms;

/**	inicializes time counter (use ISR)
 *  @param rx_isr_vect  ISR vector
 */
void init_time (unsigned rx_isr_vect);
#endif
