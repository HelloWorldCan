//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//


/**
 * @file   MC1319x.c
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  Library for MC13xx2 freescale radio
 * 
 */



#include "spi.h"
#include "MC1319x.h"
#include "MC1319xdef.h"


#ifdef LPC
	#include <lpc21xx.h>                            /* LPC21xx definitions */
	#include <types.h>
	#include <deb_led.h>
#endif	

struct Message *rcvBuf;		///< Pointer to recieve Message buffer
struct Message *sndBuf;		///< Pointer to send Message buffer


uint8_t MC_RecievePaket(struct Message *msg);



/**
 *	Interupt handling. Here is reading IRQ register and deremine what generates IRQ
 * 	@note  This function is platform depended
 */

#ifdef ATmega88 
	ISR(INT0_vect)
#endif
#ifdef 	LPC
	void ext_isr(void)
#endif
	
{	
	uint16_t dummy =0;

#ifdef LPC 
	EXTINT |= EXTINT_EINT3_m ;	// Acknowledge interupt source
#endif
	
	deb_led_on(LEDR); 
//	send_rs_str("ISR\n"); 		// printout that ISR occurs
	
	uint16_t mask = spi_Read(IRQ_STATUS);	

	if((mask & IRQ_STATUS_RX_RCVD_IRQm) == IRQ_STATUS_RX_RCVD_IRQm) // Recieved data
	{
		CLR(RXTXEN);		// clear 
		
		dummy = (spi_Read(RX_STATUS) & 0x7F);
		
		if(!(mask & 1)) 
		{
			MC_RecievePaket(rcvBuf);
			goto ISR_END;
		}

		rcvBuf->done = 1;
		rcvBuf->len = dummy;

		spi_Read_Buf(rcvBuf);
	}


	if((mask & IRQ_STATUS_TX_SENT_IRQm) == IRQ_STATUS_TX_SENT_IRQm) // Data send done
	{
		CLR(RXTXEN);		// clear 
		sndBuf->done = 1;
	}
	
	
//	if((mask & IRQ_STATUS_PLL_LOCK_IRQm) == IRQ_STATUS_PLL_LOCK_IRQm); //todo , lost PLL LOCK
//	if((mask & IRQ_STATUS_RAM_ADR_ERRm) == IRQ_STATUS_RAM_ADR_ERRm) ;	//todo, RAM buffer overrun
//	if((mask & IRQ_STATUS_ARB_BUSY_ERRm) == IRQ_STATUS_ARB_BUSY_ERRm) ;	//todo
//	if((mask & IRQ_STATUS_SRTM_DATA_ERRm) == IRQ_STATUS_SRTM_DATA_ERRm); //todo Streaming data requested/aviable
//	if((mask & IRQ_STATUS_ATTN_IRQm) == IRQ_STATUS_ATTN_IRQm); //todo ATTN asserted 
//	if((mask & IRQ_STATUS_DOZE_IRQm) == IRQ_STATUS_DOZE_IRQm); // todo	timer event - return to idle from doze 
//	if((mask & IRQ_STATUS_CCA_IRQm) == IRQ_STATUS_CCA_IRQm); //todo CCA/ED done
//	if((mask & IRQ_STATUS_TMR1_IRQm) == IRQ_STATUS_TMR1_IRQm);//todo timer 1 match
//	if((mask & IRQ_STATUS_TMR2_IRQm) == IRQ_STATUS_TMR2_IRQm);//todo timer 2 match
//	if((mask & IRQ_STATUS_TMR3_IRQm) == IRQ_STATUS_TMR3_IRQm);//todo timer 3 match
//	if((mask & IRQ_STATUS_TMR4_IRQm) == IRQ_STATUS_TMR4_IRQm);//todo timer 4 match	
//	if((mask & IRQ_STATUS_CCAm) == IRQ_STATUS_CCAm);	// output of the comparation of CCA
//	if((mask & IRQ_STATUS_CRC_VALIDm) == IRQ_STATUS_CRC_VALIDm);	// recieved correct CRC
	
ISR_END:
	
#ifdef LPC
	VICVectAddr = 0;	// acknowledge ISR in VIC
#endif
	deb_led_off(LEDR);
}
/**
 *	Delay function.
 * 	@note Should be removed in future
 */
void dummy_wait()
{
	unsigned int wait = 5000000;
	while(--wait);
}



/**
 *	Woodoo function inicialize radio. I hove no idea what it is doing, but its VERY inportatnt.
 */
void MC_Woodoo(void)
{
	spi_ReadModifiWrite(0x08, 0x0001, 0xFFFF);
	spi_ReadModifiWrite(0x08, 0x0010, 0xFFFF);
	spi_ReadModifiWrite(0x11, 0x0000, 0xFCFF);
	spi_ReadModifiWrite(0x06, 0x4000, 0xFFFF);	
}


/**
 *	Reset the radio to it default state and inicialize it
 */
uint8_t MC_Reset(void)		// generate reset on MC13192x
{

	
	disable_IRQ_pin();
		
	// reset MC radio
	SET(ATTN);
	CLR(RXTXEN);
	CLR(RST);
	dummy_wait();
	SET(RST);	


	// wait for IRQ
	while( (IO0PIN & (1<<IRQ)) == (1<<IRQ));	// waiting for incomming interrupt
	
	MC_Woodoo();	// radio init woodoo
	
	if(spi_Read(RST_IND) & RST_IND_RESET_INDm) // test reset indicator
	{
		return 1;	// return 1 if test fails
	}	

	spi_Read(IRQ_STATUS);	// acknowledge power-on IRQ
	
	spi_ReadModifiWrite( GPIO_DATA_OUT, (1<<7) | (1<<9) ,0xFFFF);  // eneble IRQ pullup and set moderate drive strenght
	spi_ReadModifiWrite( CONTROL_B, (1<<12)|(1<<14),0xFFFF);
	spi_ReadModifiWrite( CONTROL_A, CONTROL_A_RX_RCVDm|CONTROL_A_TX_SENTm,0xFFFF);

#ifdef LPC
	EXTINT |= EXTINT_EINT3_m ;  // acknowledge ISR
#endif
	
	enable_IRQ_pin();
	return 0;
}

/**
 *	Scan selected radio channel and returns measured energy  (lower is better)
 * 	@note  This function is not using ISR, but in future it may do.
 */
uint8_t MC_ED(void)
{
	uint16_t mask;
	uint8_t val;

	disable_IRQ_pin();

	CLR(RXTXEN);	//switch off radio  output
	spi_ReadModifiWrite(CONTROL_A,CONTROL_A_CCA_EDm|CONTROL_A_CCA_XCVR_CCAEDm | CONTROL_A_CCAm, \
		~(CONTROL_A_CCA_TYPE0m | CONTROL_A_CCA_XCVR_SEG1m ));
	// selects CCA as ED type and sets transerciver to CCA/ED mode and alows to generate CCA interrupt
	
	
	SET(RXTXEN);	// starts the ED detection
		
	while( (IO0PIN & (1<<IRQ)) == (1<<IRQ));	// waiting for incomming interrupt

	CLR(RXTXEN);	//switch off radio  output
	
	mask = spi_Read(IRQ_STATUS); // acknowledge interrupt
	if((mask & IRQ_STATUS_CCA_IRQm) != IRQ_STATUS_CCA_IRQm) return 1;
		
	val = (uint8_t)(spi_Read(RX_STATUS) >> 8);

	enable_IRQ_pin();

	return val;
}

/**
 *	Sends the packed stored in Message structure
 *	@param *msg	ponter to Message structure
 */
uint8_t MC_SendPaket(struct Message *msg)
{

	sndBuf = msg;
	msg->done = 0;

	CLR(RXTXEN);	//switch off radio  output

	spi_ReadModifiWrite(CONTROL_A,  CONTROL_A_TX_SENTm | CONTROL_A_RX_RCVDm \
		,~(CONTROL_A_TX_STRMm|CONTROL_A_RX_STRMm|CONTROL_A_TMR_TRIG_ENm|CONTROL_A_CCA_XCVR_CLRm)  );
			// selects CCA as ED type and sets transerciver to CCA/ED mode and alows to generate CCA interrupt
	
	spi_Write(TX_PKT_CTL, TX_PKT_CTL_PKT_LENGHT(msg->len));	// write the number data bytes in message + 2
	spi_Write_Buf (msg);	// recursive write data to MC radio

	spi_ReadModifiWrite(CONTROL_A, CONTROL_A_CCA_XCVR_PMTXm, 0xFFFF);	// Sets TX mode
	SET(RXTXEN);	// starts transmit

	return 0;
}

/**
 *	Resend the packed stored in radio transmit buffer
 *	@param *msg pointer to Message structure
 * 	@note  Not sure if it is working
 */
uint8_t MC_ReSendPaket(struct Message *msg)
{
	sndBuf = msg;
	msg->done = 0;

	CLR(RXTXEN);	//switch off radio  output

	spi_ReadModifiWrite(CONTROL_A,  CONTROL_A_TX_SENTm | CONTROL_A_RX_RCVDm \
		,~(CONTROL_A_TX_STRMm|CONTROL_A_RX_STRMm|CONTROL_A_TMR_TRIG_ENm|CONTROL_A_CCA_XCVR_CLRm)  );
			// selects CCA as ED type and sets transerciver to CCA/ED mode and alows to generate CCA interrupt
	spi_Write(TX_PKT_CTL, TX_PKT_CTL_PKT_LENGHT(msg->len));	// recursive write data to MC radio

	spi_ReadModifiWrite(CONTROL_A, CONTROL_A_CCA_XCVR_PMTXm, 0xFFFF);	// Sets TX mode

	SET(RXTXEN);	// starts transmit

	return 0;
}


/**
 *	Reads packet stored in radio buffer
 *	@param *msg	pointer to Message structure
 */
uint8_t MC_RecievePaket(struct Message *msg)
{
	uint16_t dummy;

	rcvBuf = msg;
	msg->done = 0;

	CLR(RXTXEN);	//switch off radio  output

	spi_ReadModifiWrite(CONTROL_A,  CONTROL_A_TX_SENTm | CONTROL_A_RX_RCVDm | CONTROL_A_CCA_XCVR_PMRXm \
		,~(CONTROL_A_TX_STRMm|CONTROL_A_RX_STRMm|CONTROL_A_TMR_TRIG_ENm|CONTROL_A_CCA_XCVR_CLRm)  );
			// selects CCA as ED type and sets transerciver to CCA/ED mode and alows to generate CCA interrupt

	dummy = (spi_Read(RX_STATUS) & 0x7F);
	SET(RXTXEN);	// starts recieving

	return 0;	
}


/**
 *	Returns radio identification
 */
uint16_t MC_WhoAmI(void)
{
	return spi_Read(CHIP_ID);
}

/**
 *	Sets radio frequency channel
 *	@param channel	specifies frequency output (1~16)
 * 	@note  See definions from MC1319xdef.h
 */
uint8_t MC_SetChannel(uint8_t channel)
{
	uint16_t int_div = 0;
	uint16_t lo1_num = 0;

	switch(channel)
	{
		case 1:  	int_div = LO1_INT_DIV_CH1;
					lo1_num = LO1_NUM_CH1;
					break;

		case 2:  	int_div = LO1_INT_DIV_CH2;
					lo1_num = LO1_NUM_CH2;
					break;

		case 3:  	int_div = LO1_INT_DIV_CH3;
					lo1_num = LO1_NUM_CH3;
					break;

		case 4:  	int_div = LO1_INT_DIV_CH4;
					lo1_num = LO1_NUM_CH4;
					break;

		case 5:  	int_div = LO1_INT_DIV_CH5;
					lo1_num = LO1_NUM_CH5;
					break;

		case 6:  	int_div = LO1_INT_DIV_CH6;
					lo1_num = LO1_NUM_CH6;
					break;

		case 7:  	int_div = LO1_INT_DIV_CH7;
					lo1_num = LO1_NUM_CH7;
					break;

		case 8:  	int_div = LO1_INT_DIV_CH8;
					lo1_num = LO1_NUM_CH8;
					break;

		case 9:  	int_div = LO1_INT_DIV_CH9;
					lo1_num = LO1_NUM_CH9;
					break;

		case 10:  	int_div = LO1_INT_DIV_CH10;
					lo1_num = LO1_NUM_CH10;
					break;

		case 11:  	int_div = LO1_INT_DIV_CH11;
					lo1_num = LO1_NUM_CH11;
					break;

		case 12:  	int_div = LO1_INT_DIV_CH12;
					lo1_num = LO1_NUM_CH12;
					break;

		case 13:  	int_div = LO1_INT_DIV_CH13;
					lo1_num = LO1_NUM_CH13;
					break;

		case 14:  	int_div = LO1_INT_DIV_CH14;
					lo1_num = LO1_NUM_CH14;
					break;

		case 15:  	int_div = LO1_INT_DIV_CH15;
					lo1_num = LO1_NUM_CH15;
					break;
		
		case 16:  	int_div = LO1_INT_DIV_CH16;
					lo1_num = LO1_NUM_CH16;
					break;

		default:	return 0;
	}

	spi_Write(LO1_NUM,lo1_num);
	spi_ReadModifiWrite(LO1_INT_DIV,int_div,0xFF00);

	return 1;
}

/**
 *	Sets CLKO pin to specified clock output
 *	@param tick	specifies frequency output 
 *	@param enable	enable or disable output
 * 	@note  Use definions from MC1319xdef.h
 */
uint8_t MC_SetClko(uint16_t tick, uint8_t enable)
{
	
	spi_ReadModifiWrite(CLKO_CTL,tick,~(CLKO_CTL_CLKO_CLRm));

	if (enable == 1)
	{
		spi_ReadModifiWrite(CONTROL_C,CONTROL_C_CLKO_ENm,0xFFFF);
	}
	else
	{
		spi_ReadModifiWrite(CONTROL_C,0,~(CONTROL_C_CLKO_ENm));
	}

	return 0;
}

/**
 *	Im not remeber what is this doing.
 */
uint8_t MC_SetPa(uint8_t val)
{
		spi_ReadModifiWrite(PA_LVL,(uint16_t)val,0xFFFF);
		return 0;
}
