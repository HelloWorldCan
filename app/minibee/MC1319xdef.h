//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//


/**
 * @file   MC1319xdef.h
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  Radio register definition and platform specific definitions.
 * 
 */

#ifndef MC1319XDEF_H
#define MC1319XDEF_H

// define proctype

//#define ATmega88  ///< Select ATmega88 as master MCU
#define LPC	///< Select LPC as master MCU


//------------- procesor ATmega88 specific definitons ------------

// control pins 
#ifdef ATmega88 

	#define IRQ 	PD2		///< Inverted , input, ISR request
	#define CE 	PD4		///< slave select , #CE on MC radio
	#define ATTN 	PD5		///< Inverted , output 
	#define RXTXEN 	PD6		///< enable RX or TX operations
	#define RST 	PD7		///< Inverted  , output 
	#define DD_MOSI PB3		///< SPI - master output	
	#define DD_MISO PB4  		///< SPI - master input
	#define DD_SCK 	PB5		///< SPI - master clock


	
	#define CLR(x) \		///< clear pin command 
		{ PORTD &= ~(1<<(x)); }


	#define SET(x) \	///< clear pin command 
		{ PORTD |= (1<<(x)); }
#endif

#ifdef LPC

#include <types.h>

	#define IRQ 	9		///< P0.9 Inverted , input, ISR request	
	#define CE 	7		///< P0.7 slave select , #CE on MC radio
	#define ATTN 	11		///< P0.11 Inverted , output
	#define RXTXEN 	8		///< P0.8 enable RX or TX operations
	#define RST 	12		///< P0.12 Inverted  , output
	#define MOSI 	6		///< P0.6 SPI - master output
	#define MISO 	5  		///< P0.5 SPI - master input
	#define SCK 	4		///< P0.4 SPI - master clock

	/// clear pin command 
	#define CLR(x) \
		{ IO0CLR |= (1<<(x)); }

	/// set pin command 
	#define SET(x) \
	{ IO0SET |= (1<<(x)); }

#endif


// ------------------ message definitons ----------------




/// Radio message structure 
struct Message{
	uint8_t done;		///<  Send / recieve  flag 
  	uint8_t error;		///<  error flag - ocured during send or recieveng
  	uint8_t len;		///< data length t osend (0 to 125)
  	uint8_t data[25]; 	///< data 
}  ;






//-------------- MC1319x definitions -----------------------------


// SPI read and write mask comands
#define RD		0x80
#define WR		0x00

// Reset register
#define RESET 0x00		

// Recieve buffer register
#define RX_PKT_RAM	0x01	

// Transmit buffer register
#define TX_PKT_RAM	0x02	

// Transmit control register
#define  TX_PKT_CTL	0x03	
	#define TX_PKT_CTL_RAM2_SELm 15
	#define TX_PKT_CTL_PKT_LENGHT(x) ( x + 2 )

// CCA treshold register
#define	CCA_THRESH	0x04

// IRQ mas register
#define	IRQ_MASK	0x05
	#define IRQ_MASK_ATTNm		0x8000	
	#define IRQ_MASK_RAM_ADDRm	0x1000
	#define IRQ_MASK_ARB_BUSYm	0x0800
	#define IRQ_MASK_STRM_DATAm	0x0400
	#define IRQ_MASK_PPL_LOCKm	0x0200
	#define IRQ_MASK_ACOMAm		0x0100
	#define IRQ_MASK_DOZEm		0x0010
	#define IRQ_MASK_TMR4m		0x0008
	#define IRQ_MASK_TMR3m		0x0004
	#define IRQ_MASK_TMR2m		0x0002
	#define IRQ_MASK_TMR1m		0x0001


#define CONTROL_A	0x06
	#define	CONTROL_A_TX_STRMm	0x1000
	#define	CONTROL_A_RX_STRMm	0x0800
	#define	CONTROL_A_CCAm		0x0400
	#define	CONTROL_A_TX_SENTm	0x0200
	#define	CONTROL_A_RX_RCVDm	0x0100
	#define	CONTROL_A_TMR_TRIG_ENm	0x0080
	#define	CONTROL_A_CCA_TYPE1m	0x0020
	#define	CONTROL_A_CCA_TYPE0m	0x0010
	#define	CONTROL_A_CCA_CCAm	CONTROL_A_CCA_TYPE0m
	#define	CONTROL_A_CCA_EDm	CONTROL_A_CCA_TYPE1m
	#define	CONTROL_A_CCA_XCVR_SEG1m	0x0002
	#define	CONTROL_A_CCA_XCVR_SEG0m	0x0001
	#define	CONTROL_A_CCA_XCVR_IDLEm	0
	#define	CONTROL_A_CCA_XCVR_CCAEDm	CONTROL_A_CCA_XCVR_SEG0m
	#define	CONTROL_A_CCA_XCVR_PMRXm	CONTROL_A_CCA_XCVR_SEG1m
	#define	CONTROL_A_CCA_XCVR_PMTXm	(CONTROL_A_CCA_XCVR_SEG1m | CONTROL_A_CCA_XCVR_SEG0m)
	#define	CONTROL_A_CCA_XCVR_CLRm	(CONTROL_A_CCA_XCVR_SEG1m | CONTROL_A_CCA_XCVR_SEG0m)

#define CONTROL_B	0x07
	#define	CONTROL_B_TMR_LOADm	0x8000
	#define	CONTROL_B_MISO_HIZ_ENm	0x0800
	#define	CONTROL_B_CLKO_DOZE_ENm	0x0200
	#define	CONTROL_B_TX_DONEm	0x0080
	#define	CONTROL_B_RX_DONEm	0x0040
	#define	CONTROL_B_USE_STM_MODEm	0x0020
	#define	CONTROL_B_HIB_ENm	0x0002
	#define	CONTROL_B_DOZE_ENm	0x0001

#define	PA_ENABLE	0x08
	#define PA_ENABLE_PA_ENm	0x8000

#define CONTROL_C	0x09
	#define	CONTROL_C_GPIO_ALT_ENm	0x0080
	#define	CONTROL_C_CLKO_ENm	0x0020
	#define	CONTROL_C_TMR_PRESCALE2m	0x0004
	#define	CONTROL_C_TMR_PRESCALE1m	0x0002
	#define	CONTROL_C_TMR_PRESCALE0m	0x0001

#define	CLKO_CTL	0x0A
	#define	CLKO_CTL_CLKO_RATE2m	0x0004
	#define	CLKO_CTL_CLKO_RATE1m	0x0002
	#define	CLKO_CTL_CLKO_RATE0m	0x0001
	#define	CLKO_CTL_CLKO_16Mm	0x00
	#define	CLKO_CTL_CLKO_8Mm	CLKO_CTL_CLKO_RATE0m
	#define	CLKO_CTL_CLKO_4Mm	CLKO_CTL_CLKO_RATE1m
	#define	CLKO_CTL_CLKO_2Mm	(CLKO_CTL_CLKO_RATE0m | CLKO_CTL_CLKO_RATE1m)
	#define	CLKO_CTL_CLKO_1Mm	CLKO_CTL_CLKO_RATE2m
	#define	CLKO_CTL_CLKO_62Km	(CLKO_CTL_CLKO_RATE0m | CLKO_CTL_CLKO_RATE2m)
	#define	CLKO_CTL_CLKO_32Km	(CLKO_CTL_CLKO_RATE1m | CLKO_CTL_CLKO_RATE2m)
	#define	CLKO_CTL_CLKO_16Km	(CLKO_CTL_CLKO_RATE0m | CLKO_CTL_CLKO_RATE1m| CLKO_CTL_CLKO_RATE2m)

	#define	CLKO_CTL_CLKO_ONm	0x01	// additional definition, not in register
	#define	CLKO_CTL_CLKO_OFFm	0x00	// additional definition, not in register
	#define	CLKO_CTL_CLKO_CLRm	(CLKO_CTL_CLKO_RATE0m | CLKO_CTL_CLKO_RATE1m| CLKO_CTL_CLKO_RATE2m)


#define GPIO_DIR	0x0B	// not finished
#define GPIO_DATA_OUT	0x0C	// not finished


#define	LO1_INT_DIV	0x0F
	#define LO1_INT_DIV_CH1 0x95
	#define LO1_INT_DIV_CH2 0x95
	#define LO1_INT_DIV_CH3 0x95
	#define LO1_INT_DIV_CH4 0x96
	#define LO1_INT_DIV_CH5 0x96
	#define LO1_INT_DIV_CH6 0x96
	#define LO1_INT_DIV_CH7 0x97
	#define LO1_INT_DIV_CH8 0x97
	#define LO1_INT_DIV_CH9 0x97
	#define LO1_INT_DIV_CH10 0x98
	#define LO1_INT_DIV_CH11 0x98
	#define LO1_INT_DIV_CH12 0x98
	#define LO1_INT_DIV_CH13 0x99
	#define LO1_INT_DIV_CH14 0x99
	#define LO1_INT_DIV_CH15 0x99
	#define LO1_INT_DIV_CH16 0x9A
	

#define	LO1_NUM	0x10
	#define	LO1_NUM_CH1	0x5000
	#define	LO1_NUM_CH2	0xA000
	#define	LO1_NUM_CH3	0xF000
	#define	LO1_NUM_CH4	0x4000
	#define	LO1_NUM_CH5	0x9000
	#define	LO1_NUM_CH6	0xE000
	#define	LO1_NUM_CH7	0x3000
	#define	LO1_NUM_CH8	0x8000
	#define	LO1_NUM_CH9	0xD000
	#define	LO1_NUM_CH10	0x2000
	#define	LO1_NUM_CH11	0x7000
	#define	LO1_NUM_CH12	0xC000
	#define	LO1_NUM_CH13	0x1000
	#define	LO1_NUM_CH14	0x6000
	#define	LO1_NUM_CH15	0xB000
	#define	LO1_NUM_CH16	0x0000
	
	
// channel decimal definiton		
#define ZB_CH1	1
#define ZB_CH2	2
#define ZB_CH3	3
#define ZB_CH4	4
#define ZB_CH5	5
#define ZB_CH6	6
#define ZB_CH7	7
#define ZB_CH8	8
#define ZB_CH9	9
#define ZB_CH10	10
#define ZB_CH11	11
#define ZB_CH12	12
#define ZB_CH13	13
#define ZB_CH14	14
#define ZB_CH15	15
#define ZB_CH16	16

// channel 802.15.4 definiton
#define ZB_CH802_11	ZB_CH1
#define ZB_CH802_12	ZB_CH2
#define ZB_CH802_13	ZB_CH3
#define ZB_CH802_14	ZB_CH4
#define ZB_CH802_15	ZB_CH5
#define ZB_CH802_16	ZB_CH6
#define ZB_CH802_17	ZB_CH7
#define ZB_CH802_18	ZB_CH8
#define ZB_CH802_19	ZB_CH9
#define ZB_CH802_20	ZB_CH10
#define ZB_CH802_21	ZB_CH11
#define ZB_CH802_22	ZB_CH12
#define ZB_CH802_23	ZB_CH13
#define ZB_CH802_24	ZB_CH14
#define ZB_CH802_25	ZB_CH15
#define ZB_CH802_26	ZB_CH16




#define PA_LVL	0x12// not finished

#define TMR_CMP1_A	0x1B// not finished
#define TMR_CMP1_B	0x1C// not finished

#define TMR_CMP2_A	0x1D// not finished
#define TMR_CMP2_B	0x1E// not finished

#define TMR_CMP3_A	0x1F// not finished
#define TMR_CMP3_B	0x20// not finished

#define TMR_CMP4_A	0x21// not finished
#define TMR_CMP4_B	0x22// not finished

#define TC2_PRIME	0x23// not finished

#define IRQ_STATUS	0x24
	#define IRQ_STATUS_PLL_LOCK_IRQm	0x8000
	#define IRQ_STATUS_RAM_ADR_ERRm	0x4000
	#define IRQ_STATUS_ARB_BUSY_ERRm	0x2000
	#define IRQ_STATUS_SRTM_DATA_ERRm	0x1000
	#define IRQ_STATUS_ATTN_IRQm	0x0400
	#define IRQ_STATUS_DOZE_IRQm	0x0200
	#define IRQ_STATUS_TMR1_IRQm	0x0100
	#define IRQ_STATUS_RX_RCVD_IRQm	0x0080
	#define IRQ_STATUS_TX_SENT_IRQm	0x0040
	#define IRQ_STATUS_CCA_IRQm	0x0020
	#define IRQ_STATUS_TMR3_IRQm	0x0010
	#define IRQ_STATUS_TMR4_IRQm	0x0008
	#define IRQ_STATUS_TMR2_IRQm	0x0004
	#define IRQ_STATUS_CCAm	0x0002
	#define IRQ_STATUS_CRC_VALIDm	0x0001

#define	RST_IND	0x25
	#define RST_IND_RESET_INDm	0x0080


#define	CURRENT_TIME_A	0x26
#define	CURRENT_TIME_B	0x27

#define	GPIO_DATA_IN	0x28	// not finished

#define	CHIP_ID	0x2C	// not finished

#define	RX_STATUS	0x2D

#define	TIMESTAMP_A	0x2E
#define	TIMESTAMP_B	0x2F

#define	BER_ENABLE	0x30
	#define	BER_ENABLE_BER_EN	0x8000 





#endif



















