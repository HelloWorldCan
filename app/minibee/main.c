//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//


/**
 * \mainpage Zigbee radio MC13192 or MC13202
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * \section Introduction
 * 	This code provides same sample how to use MC13xx2 radio in packet mode.
 * 	The basic libraries for testing are writen mostly platform independent. 
 * 	See the function description. In some function is noted that use some platform 
 * 	depended parts whis should be modified. Originaly this code was developed for 
 * 	MC13192 radio, but now we use MC13202 radio which is almost fully compatible.
 * 
 * 	MC1319x.c - is mostly platform independent, see notes inside. This library 
 * 	contains basic function which contains funcion for send, recieve, set clko, 
 * 	read ID, set frequency and do energy detect.
 * 
 * 	MC1319xdef.h - contains register definions and bit masks for MC13xxx radio
 * 
 * 	spi.h - platform independent. Contains function prototypes and definition 
 * 	SPI_SPEED for setting SPI speed in Hz.
 * 
 * 	spi_LPC.c - platform dependent. Contains inicializing function for SPI channel,
 * 	all other used pins and installs ISR.
 * 
 * 	uart_minibee.h	- platform independent. Function prototypes for uart communication
 * 	
 * 	uart_minibee.c	- particulary platform dependent. Some part of this code must be ported 
 * 	for new platform.
 * 	
 *	Porting to another platform:
 * 		For another platform you must implement your own SPI function, 
 * 		set pins and install ISR. It must support the prototypes functions 
 * 		which is defined in spi.h. You also must implement your
 * 		function for uart communication (see uart_minibee.c).
 * 
 * 	The last tested platform is LpcEurobot which is ARM7 micproporcesor LPC2119. 
 * 	It also use eurobot ebb library for UART communication for ARM7. Older platform 
 * 	was AVR which is no longer supported due to no platform for testing. In this code 
 * 	is added spi_avr.c which is not fully ported.
 * 
 * 	@note	Known issues:
 * 			1) IRQ edge sensive - in this code are parts where is ISR at IRQ pins is disabled. 
 * 			If this code didnt fully acknowledge IRQ in radio the renewed ISR will not recognise
 * 			IRQ request. If this occurs the main MCU must be reseted. 
 * 				Solutions:
 * 					a) comment  line 73 in spi_LPC.c - in commnent is  ***
 * 					b) move all code to ISR and never disable ISR on IRQ pin - recomended
 */

/**
 * @file   main.c
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  Demo application demonstrating use of MC1319x library.
 * 	This file provides simply how-to use MC1319x library.
 * 	From main function is called init function of SPI and UART. 
 * 	After sucessfull inicialize it tries read chip ID and recognize
 * 	connected radio. Whan everythig is sucessfull it runs command 
 * 	processor which allows you to test radio functions.
 * 
 */






#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <deb_led.h>
#include <types.h>
#include "MC1319x.h"
#include "spi.h"
#include "uart_minibee.h"



struct Message MsgSnd;	///< Send buffer
struct Message MsgRcv;	///< Recieve buffer




/**
 *	Send data via radio
 *	@param *msg	pointer to Message sructure
 */
void sendData(struct Message *msg)
{
	msg->done = 0;
	msg->error = 0;
	MC_SendPaket(msg);
}

/**
 *	Reads data from Radio buffer
 */
void recieveData(void)
{
	MsgRcv.done = 0;
	MsgRcv.error = 0;

	MC_RecievePaket(&MsgRcv);
}
/**
 *	At first it inicializes serial and SPI line + other pins + ISR (RXTXEN, IRQ....) 
 * 	then it inicialzes Radio and reads radio ID. Whan everything is sucessfull 
 * 	runs the serial command processor.
 *	
 */

int main (void)  {

	volatile uint32_t test =0;	

	// init serial line 
	init_uart();

	// init SPI line, ISR and oter pins
	send_rs_str("SPI init.\n");
	spi_Init(5);
	
	// reset and inicialize radio
	send_rs_str("\nMC reset\n");
	if (MC_Reset()==1) 
	{
		send_rs_str("FAIL\n");
		while(1);
	}	
	
	// Read and test radio ID
	send_rs_str("Who am i:\n");
	test = MC_WhoAmI();
	if((test == 0x5000) | (test == 0x5400))
	{
		send_rs_str("MC13192 Detected\n");
	}
	else if ((test == 0x6000) | (test == 0x6400))
		send_rs_str("MC13202 Detected\n");
	else 
	{
		send_rs_str("ID FAIL: ");
		send_rs_int(test);
		while(1);
	}


	MC_SetPa(0xFF);			// nastavi vysilaci vykon
	MC_SetChannel(ZB_CH802_21);	// nastavi kanal
	
	

	int command = 0;
	int myvar = 0;
	int j = 0;
	send_rs_str("Entering command processor loop\n");	
	
	while(1)
	{
		send_rs_str("-----------------------------------\n");
		send_rs_str("1 Send message\n");
		send_rs_str("2 Recieve message\n");
		send_rs_str("3 Get IRQ pin\n");
		send_rs_str("4 enable ISR\n");
		send_rs_str("5 Read IRQ\n");
		send_rs_str("6 Set channel\n");
		send_rs_str("7 Energy detect\n");
		send_rs_str("8 Set clock output\n");
		send_rs_str("-----------------------------------\n");
				
		command = uart_get_char();
		uart_send_char(command);
		uart_send_char('\n');
		
		switch(command){
			case '1':
				send_rs_str("Sending message\n");
				MsgSnd.len = 14;
				MsgSnd.data[0] = 0x88;
				MsgSnd.data[1] = 0x41;
				MsgSnd.data[2] = 0x22;
				MsgSnd.data[3] = 0x88;
				MsgSnd.data[4] = 0xFF;
				MsgSnd.data[5] = 0x00;
				MsgSnd.data[6] = 0x01;
				MsgSnd.data[7] = 0xFF;
				MsgSnd.data[8] = 0x06;
				MsgSnd.data[9] = 0x00;
				MsgSnd.data[10] = 0x01;
				MsgSnd.data[11] = 0x00;
				MsgSnd.data[12] = 0x89;
				MsgSnd.data[13] = 0x00;
				sendData(&MsgSnd);	
				break;
				
			case '5':
				disable_IRQ_pin();
				send_rs_str("IRQ register is: ");
				myvar = spi_Read(IRQ_STATUS);	// acknowledge power-on IRQ	
				send_rs_int(myvar);
				enable_IRQ_pin();
				break;
				
			case '3':
				send_rs_str("IRQ pin is: ");
				if((IO0PIN & (1<<IRQ)) == (1<<IRQ))
					send_rs_str("true\n");	
				else
					send_rs_str("false\n");
				break;
				
			case '4': send_rs_str("Enable ISR: OK\n");
				enable_IRQ_pin();
				break;
				
			case '2': send_rs_str("Recieving:\n");
				recieveData();
				myvar = 0;
				while(myvar<10)
				{
					if((MsgRcv.done == 1))
					{
						send_rs_str("Recieved Data:\n");
						send_rs_str("Msg.len: ");
						send_rs_int(MsgRcv.len);
						send_rs_str("\nMsg.data: \n");
						for (j = 0; j < MsgRcv.len; j++)
						{
							send_rs_int(MsgRcv.data[j]);
							send_rs_str("\n");				
						}
						
						break;
					}
					else{
						++myvar;
						dummy_wait();
					}
				}
				
				if(myvar == 10)
					send_rs_str("Timed out\n");		
				else
					send_rs_str("Reading done!\n");		
				break;
				
			case '6':
				send_rs_str("Select channel (11~26)\n");
				j = uart_get_char();
				uart_send_char(j);
				
				if(!((j == '1')|(j=='2'))){			
					send_rs_str("Bad value\n");
					break;
				}
				
				myvar = (j - '0')* 10;
				
				j = uart_get_char();
				uart_send_char(j);
				if(!((j >= '0')&(j<='9'))){
					send_rs_str("Bad value\n");
					break;
				}
				myvar += (j - '0');
				myvar -= 10;
				MC_SetChannel(myvar);
				break;
				
			case '7':
				send_rs_str("Energy detect: ");
				send_rs_int(MC_ED());
				break;
				
			case '8':
				send_rs_str("Enable CLKO? (y/n) ");
				j = uart_get_char();
				uart_send_char(j);
				if( j == 'n')
				{
					send_rs_str("CLKO - Switching OFF");	
					MC_SetClko(CLKO_CTL_CLKO_16Km, CLKO_CTL_CLKO_OFFm);
					break;
				}
				else if( j != 'y')
				{
					send_rs_str("\nInvalid command\n");	
					break;
				}
				
				
				send_rs_str("Select clock\n");
				send_rs_str("1 16.393kHz\n");
				send_rs_str("2 32.786kHz\n");
				send_rs_str("3 62.5kHz\n");
				send_rs_str("4 1Mhz\n");
				send_rs_str("5 2Mhz\n");
				send_rs_str("6 4Mhz\n");
				send_rs_str("7 8Mhz\n");
				send_rs_str("8 16Mhz\n");
					
				j = uart_get_char();
				uart_send_char(j);
					
				if(('0' < j) & (j < '9'))
				{
					switch(j){
						case '1':
							MC_SetClko(CLKO_CTL_CLKO_16Km, CLKO_CTL_CLKO_ONm);
							break;
						case '2':
							MC_SetClko(CLKO_CTL_CLKO_32Km, CLKO_CTL_CLKO_ONm);
							break;
						case '3':
							MC_SetClko(CLKO_CTL_CLKO_62Km, CLKO_CTL_CLKO_ONm);
							break;
						case '4':
							MC_SetClko(CLKO_CTL_CLKO_1Mm, CLKO_CTL_CLKO_ONm);
							break;
						case '5':
							MC_SetClko(CLKO_CTL_CLKO_2Mm, CLKO_CTL_CLKO_ONm);
							break;
						case '6':
							MC_SetClko(CLKO_CTL_CLKO_4Mm, CLKO_CTL_CLKO_ONm);
							break;
						case '7':
							MC_SetClko(CLKO_CTL_CLKO_8Mm, CLKO_CTL_CLKO_ONm);
							break;
						case '8':
							MC_SetClko(CLKO_CTL_CLKO_16Mm, CLKO_CTL_CLKO_ONm);
							break;
						default:
							break;
					}
				}
				else
				{
					
					send_rs_str("Invalid command\n");
				}
				break;
				
				
			default:
				send_rs_str("Bad command!\n");
				break;
				
		}
		
		dummy_wait();
		
		// test if sessage is sent
		if ((MsgSnd.done == 1))
		{
			send_rs_str("Message sent!!!!!!!!!!!!!!\n");
			MsgSnd.done = 0;
			// add same handle code her
		}

		// test if some packet is recieved
		if ((MsgRcv.done == 1))
		{
			// add same handle code here
		}
		
		
		
		send_rs_str("\nHit key to continue ...");
		uart_get_char();
		send_rs_str("\n\n\n\n\n\n\n\n\n\n");
	}

}



