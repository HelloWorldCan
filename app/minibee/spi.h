//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//



/**
 * @file   spi_LPC.c
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  SPI, IRQ and pin inicialization function prototypes
 * 	A new platform must support this functions.
 * 
 */


#ifndef SPI_LPC_H
#define SPI_LPC_H

#include "MC1319xdef.h"

#define SPI_SPEED 200	///< SPI frequency in  Hz

void spi_Init(int rx_isr_vect);		// inits SPI and hold MC radio in reset state
uint16_t spi_Read (uint8_t reg);	// reads 16 bites from given register
void spi_Write (uint8_t reg, uint16_t val); // writes 16 bites from given register
void spi_ReadModifiWrite (uint8_t reg, uint16_t ormask, uint16_t andmask); // reads_modify-writes 16 bites from given register
void spi_Write_Buf (struct Message *msg); // fill MC transimit buffer
void spi_Read_Buf (struct Message *msg);
uint8_t MC_SetChannel(uint8_t channel);
void disable_IRQ_pin(void);
void enable_IRQ_pin(void);

#endif
