//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//

/**
 * @file   spi_LPC.c
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  SPI, IRQ and pin inicialization, platform dependent!
 * 	Contains support functions for SPI communiacations
 * 
 */

#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <types.h>
#include "MC1319x.h"
#include <system_def.h>
#include "spi.h"


/**
 *	Disabling interupt from radio IRQ pin
 */
void disable_IRQ_pin(void)
{
	VICIntEnClr |= 1<<VIC_EINT3;			
}

/**
 *	Enabling interupt from radio IRQ pin
 */
void enable_IRQ_pin(void)
{
	VICIntEnable |= 1<<VIC_EINT3;			
}


/**
 *	Inits SPI channel to speed given in  SPI_SPEED. 
 * 	Also preset other pins (ATTN,IRQ, TXRXEN, ATTN).
 *	Initiates ISR for IRQ PIn, but it is not enabled.
 */
void spi_Init(int rx_isr_vect)		// inits SPI and hold MC radio in reset state
{

	// sets pins for communication

	SET_PIN(PINSEL0, RST, PINSEL_0);	// RST as GPIO
	SET_PIN(PINSEL0, ATTN, PINSEL_0);	// ATTN as GPIO
	SET_PIN(PINSEL0, SCK, PINSEL_1);	// SCK as SPI SCK
	SET_PIN(PINSEL0, MISO, PINSEL_1);	// MISO as SPI MISO
	SET_PIN(PINSEL0, MOSI, PINSEL_1);	// MOSI as SPI MOSI
	SET_PIN(PINSEL0, CE, PINSEL_0);		// CE as GPIO 
	SET_PIN(PINSEL0, RXTXEN, PINSEL_0);	// RXTXEN as GPIO
	SET_PIN(PINSEL0, IRQ, PINSEL_3);	// IRQ as EINT3
	
	// setting SPI port
	// sets MOSI and CLK as output
	
	S0SPCR = SPCR_MSTR_m ; 			// Set SPI as Master , CPOl = CPHA =0
	
	int pom = CPU_APB_HZ /  SPI_SPEED ;	// Hz;
	if (pom%2) ++pom ;			// align to even number
	S0SPCCR = pom;				// set speed
	
	
	IO0DIR  |= (1<<ATTN)|(1<<RXTXEN)|(1<<RST)| (1<<CE); // sete control bits to output	

	
	EXTMODE |= EXTMODE_EXTMODE3_m;		// Sets EINT3 to falling edge senstive	***
	EXTINT = EXTINT_EINT3_m;			// clear interrupt
	
	
	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned long)ext_isr;	// Nastaveni adresy vektotu preruseni
	((uint32_t*)&VICVectCntl0)[rx_isr_vect] =  VIC_EINT3 | VIC_ENABLE;	// vyber EXTINT3 pro preruseni
	
}


/**
 *	Reads one register in radio
 *	@param reg	register to read
 *
 * 	@return		value in rgister
 */
uint16_t spi_Read (uint8_t reg)
{
	uint16_t data =0;

	reg &= 0x3F;	// protection before too high value

	CLR(CE);

	S0SPDR = reg | RD;// zapis prvniho byte	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

	S0SPDR = 0;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	data = (S0SPDR << 8);

	S0SPDR = 0;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	data |= S0SPDR;
	
	SET(CE);
	return data;
}


/**
 *	Write value  to one register in radio
 *	@param reg	register to write
 *	@param val	value to write
 */
void spi_Write (uint8_t reg, uint16_t val) // writes 16 bites from given register
{
	reg &= 0x3F;	// protection before too high value

	CLR(CE);
	
	S0SPDR = reg | WR;// zapis prvniho byte	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

	S0SPDR = (val>>8) & 0xFF;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	
	S0SPDR = val & 0xFF;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);	
	
	SET(CE);
}



/**
 *	Read recieved data to Message structure
 * 
 *	@param *msg	pointer to Messge structure
 */
void spi_Read_Buf (struct Message *msg) // write message to MC radio message buffer
{
	
	uint8_t i =0;
	uint8_t dummy =0;
	

	CLR(CE);
	
	S0SPDR = RX_PKT_RAM | RD;			// init write	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);



	S0SPDR = 0x00;	// dummy read
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	S0SPDR = 0x00;	// dummy read
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

	for( i = 0; i < (msg->len - 2); i++)
	{

		S0SPDR = 0;		// cteni
		while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

		dummy = S0SPDR;

		if ((i % 2) == 0) msg->data[i + 1] = dummy;
		else msg->data[i - 1] = dummy;
	}
	
	if((msg->len % 2) ==1) 
	{	
		S0SPDR = 0x00;	// dummy read
		while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	}
	
	SET(CE);
}


/**
 *	Write messgage to internal radio buffer 
 *	@param *msg	pointer to Message structure
 */
void spi_Write_Buf (struct Message *msg) // write message to MC radio message buffer
{
	uint8_t i =0;

	CLR(CE);
	
	S0SPDR = TX_PKT_RAM | WR;			// init write	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);


	for( i = 0; i < msg->len; i++)
	{
		S0SPDR = msg->data[i];		// zapis druhyho byte a cteni
		while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	}
	
	
	if((msg->len % 2) ==1) 
	{
		S0SPDR = 0x00;	// dummy write
		while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	}
	
	SET(CE);
}




/**
 *	Read-modify write function for modifiing registers in radio
 *	@param reg	register to modify
 *	@param ormask	OR mask
 * 	@param andmask	AND mask
 */
void spi_ReadModifiWrite (uint8_t reg, uint16_t ormask, uint16_t andmask) // writes 16 bites from given register
{	
	uint16_t data =0;

	reg &= 0x3F;	// protection before too high value

	CLR(CE);
	
	S0SPDR = reg | RD;// zapis prvniho byte	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

	S0SPDR = 0;		// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	data = (S0SPDR << 8);
	
	S0SPDR = 0;		// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
	data |= S0SPDR;
	
	SET(CE);

	data &= andmask;
	data |= ormask;

	CLR(CE);
	
	S0SPDR = reg | WR;// zapis prvniho byte	
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);

	S0SPDR = (data>>8) & 0xFF;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
		
	
	S0SPDR = data & 0xFF;	// zapis druhyho byte a cteni
	while((S0SPSR & SPSR_SPIF_m) != SPSR_SPIF_m);
		
	SET(CE);
}
