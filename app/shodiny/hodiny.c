/* ustredni hodiny Spejbla */

#include <types.h>
#include <lpc21xx.h>
#include <periph/can.h>

#define CANLOAD_ID (*((volatile unsigned long *) 0x40000120))

void timer_irq() __attribute__((interrupt));

void timer_init(uint32_t prescale, uint32_t period) {
  T0PR = prescale - 1;
  T0MR1 = period - 1;
  T0MCR = 0x3<<3; /* interrupt and counter reset on match1 */
  T0EMR = 0x1<<6 | 0x2; /* set MAT0.1 low on match and high now */
  T0CCR = 0x0; /* no capture */
  T0TCR |= 0x1; /* go! */
}

void timer_init_irq(unsigned irq_vect) {
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)timer_irq;
  ((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | 4;
  /* enable timer int */
  VICIntEnable = 0x00000010;
}

can_msg_t msg = {.flags = 0, .dlc = 1};
uint8_t seq_num = 0;

void timer_irq() {
  msg.data[0] = seq_num++;
  while (can_tx_msg(&msg));
  /* clear int flag */
  T0IR = 0xffffffff;
  /* int acknowledge */
  VICVectAddr = 0;
}

int main() {
  /* peripheral clock = CPU clock (10MHz) */
  VPBDIV = 1;
  /** map exception handling to RAM **/
  MEMMAP = 0x2;
  /* init Vector Interrupt Controller */
  VICIntEnClr = 0xFFFFFFFF;
  VICIntSelect = 0x00000000;
  /* 10MHz VPB, 1000kb/s TSEG1=6, TSEG2=3, SJW= */
  can_init(0x250000, 14, NULL);
  msg.id = CANLOAD_ID;
  /* * */
  timer_init(10, 4000 /*250Hz*/);
  timer_init_irq(13);
  /* jamais fatigue */
  for (;;);
}
