/* LPC21xx definitions */
#include <lpc21xx.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>

#define CAN_ID 0x401
/* can handle*/
vca_handle_t can;
uint32_t btr;
/*own can message*/
canmsg_t msg = {.id = CAN_ID, .flags = 0, .length = 1};

uint8_t seq_num = 0;

void timer_irq() __attribute__((interrupt));

void timer_init(uint32_t prescale, uint32_t period) {
  T0PR = prescale - 1;
  T0MR1 = period - 1;
  T0MCR = 0x3<<3; /* interrupt and counter reset on match1 */
  T0EMR = 0x1<<6 | 0x2; /* set MAT0.1 low on match and high now */
  T0CCR = 0x0; /* no capture */
  T0TCR |= 0x1; /* go! */
}

void timer_init_irq(unsigned irq_vect) {
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)timer_irq;
  ((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | 4;
  /* enable timer int */
  VICIntEnable = 0x00000010;
}

void timer_irq() {
  msg.data[0] = seq_num++;
  vca_send_msg_seq(can,&msg,1);
  /* clear int flag */
  T0IR = 0xffffffff;
  /* int acknowledge */
  VICVectAddr = 0;
}

/**
 * Main function 
 */
int main (void)  {

	//MEMMAP = 0x1;	//flash
	//MEMMAP = 0x2;	//ram
	VPBDIV = 1;
	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0x00000000;	

	/* CAN bus setup */
	lpcan_btr(&btr, 1000000 /*Bd*/, 10000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
	lpc_vca_open_handle(&can, 0/*device*/, 1/*flags*/,btr, 10, 11, 12);	

	timer_init(1000, 4000 /*250Hz*/);
	timer_init_irq(13);

	for(;;);
}



