#include <stdlib.h>
#include <types.h>
#include <limits.h>
#include <lpc21xx.h>
#include <cpu_def.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>
//
#include <string.h>
#include "led.h"
#include "pwm.h"

/* CAN message IDs */
#define CAN_CLOCK_ID  0x401
//#define CAN_CLOCK_ID  0x481
#define CAN_CFGMSG_ID 0x4ab 
#define CAN_UNDEF_ID  0xf800

#define CANLOAD_ID 0x481 
//(*((volatile unsigned long *) 0x40000120))
#define MOTOR_ID CANLOAD_ID

/* PWM and sampling timing */
#define PWM_PERIOD   3000
#define PWM_DEAD     60
//#define PWM_DEAD     0
#define ADC_OVERSAMP 6
#define ADC_MA_LEN   (2*ADC_OVERSAMP)

/* CAN timing */
#define CAN_OVERSAMP 80

/* position limits */
#define POS_MIN 0x060
#define POS_MAX 0x3a0

/* input channels */
#define ADCIN_POS 0
#define ADCIN_CURRENT 1

#define adc_setup(src,clkdiv,on,start,edge) \
  (((src)&0x7) | ((((clkdiv)-1)&0xff)<<8) | ((!(!(on)))<<21) | \
  (((start)&0x7)<<24) | ((!(!(edge)))<<27))

/* A/D ISR <=> controller function variables */
volatile uint8_t timer_count = 0;
volatile uint32_t adc_i = 0;
volatile uint32_t adc_x = 0;
volatile int8_t current_negative = 0;

/* reserved value for power-off */
#define CTRL_OFF 0xffff
#define CTRL_MAX SHRT_MAX

/* can handle*/
vca_handle_t can;
/* own message for current position signalling */
canmsg_t tx_msg = {.flags = 0, .length = 8};
canmsg_t rx_msg;

/* command message ID and index */
uint16_t cmd_msg_id = 0x440, cmd_msg_ndx = 0; //puvodne cmd_msg_id = CAN_UNDEF_ID;

/* command value, current position */
volatile int16_t rx_cmd_value = CTRL_OFF;
volatile uint8_t tx_request = 0;

/* * * * */

void adc_irq() __attribute__((interrupt));

void timer_init() {
  /* zapni MAT0.1 na noze AIN0 */

  T0PR = 0; //prescale - 1
  T0MR1 = PWM_PERIOD/ADC_OVERSAMP-1; //period
  T0MCR = 0x2<<3; /* counter reset on match1 */
  T0EMR = 0x1<<6 | 0x2; /* MAT0.1 set low on match */
  T0CCR = 0x0; /* no capture */
  T0TCR |= 0x1; /* go! */

  sync_pwm_timer((uint32_t*)&T0TC);
}

void set_irq_handler(uint8_t source, uint8_t irq_vect, void (*handler)()) {
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)handler;
  ((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | source;
  /* enable interrupt */
  VICIntEnable = 1<<source;
}

inline void adc_init() {
  ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x4 /*MAT0.1*/, 1 /*FALL! edge*/);
}

/************/

void hbridge_half_set(uint8_t side, uint32_t value) {
  uint32_t *s_down, *d_down, *d_up;
  uint32_t t;

  if (side) {
    s_down = PWM_MR[6];  d_down = PWM_MR[4];  d_up = PWM_MR[3];
  }
  else {
    s_down = PWM_MR[5];  d_down = PWM_MR[2];  d_up = PWM_MR[1];
  }

  if (value < 2*PWM_DEAD) {
    /* 0% */
    *s_down = PWM_PERIOD;
    *d_down = PWM_PERIOD+1;
    *d_up = 0;
  }
  else if (value > PWM_PERIOD-2*PWM_DEAD) {
    /* 100% */
    *s_down = *d_down = 0;
    *d_up = PWM_PERIOD;
  }
  else {
    *d_up = t = PWM_PERIOD-PWM_DEAD;
    //*d_down = t -= value;
    /****** !!!!!!!!! *******/
    *d_down = t -= value - 2*PWM_DEAD;
    *s_down = t - PWM_DEAD;
  }

  PWMLER |= side ? 0x58 : 0x26;
}

void hbridge_set(int32_t value) {
  if (value >= 0) {
    hbridge_half_set(0, value);
    hbridge_half_set(1, 0);
  }
  else {
    hbridge_half_set(1, -value);
    hbridge_half_set(0, 0);
  }
}

void hbridge_off() {
  /* *s_down = 0 (L) */
  PWMMR5 = 0;
  PWMMR6 = 0;
  /* *d_down = PWM_PERIOD+1, *d_up = 0 (H) */
  PWMMR4 = PWM_PERIOD+1;
  PWMMR3 = 0;
  PWMMR2 = PWM_PERIOD+1;
  PWMMR1 = 0;
  PWMLER |= 0x58 | 0x26;
}

void hbridge_init() {
  /* PWM2,4 double-edged, PWM5,6 single-edged */
  pwm_channel(2, 1);
  pwm_channel(4, 1);
  pwm_channel(5, 0);
  pwm_channel(6, 0);
  /* both sides to GND */
  //hbridge_half_set(0, 0);
  //hbridge_half_set(1, 0);
  /* disconnect the bridge */
  hbridge_off();
  /* go */
  pwm_init(1, PWM_PERIOD);
}

/*****/

//#define CTRL_INT_DIV  (1<<19)
#define CTRL_INT_DIV  (1<<15)
#define CTRL_INT_MAX  (CTRL_INT_DIV*PWM_PERIOD)
#define CTRL_AMP_MUL  (20.0/(12*(1<<11)))
#define CTRL_PI_Q     ((4.0/7.0)*CTRL_INT_MAX*CTRL_AMP_MUL)
#define CTRL_PI_K     (0.6*CTRL_PI_Q)
#define CTRL_PI_Kn    (0.6*0.674*CTRL_PI_Q)

inline int32_t add_sat(int32_t big, int32_t small, int32_t min, int32_t max) {
  if (big >= max - small)
    return max;
  if (big <= min - small)
    return min;
  return big + small;
}

int32_t pi_ctrl(int16_t w, int16_t y) {
  static int32_t s = 0;
  int32_t p, e, u, q;

  e = w;  e -= y;
  p = e;  p *= (int32_t)CTRL_PI_K;
  u = add_sat(p, s, -CTRL_INT_MAX, CTRL_INT_MAX);
  q = e;  q *= (int32_t)(CTRL_PI_K-CTRL_PI_Kn);
  s = add_sat(q, s, -CTRL_INT_MAX-p, CTRL_INT_MAX-p);

  return u/CTRL_INT_DIV;
}


int32_t pi_ctrl_(int16_t w, int16_t y) {
  static int32_t z = 0, intg = 0;
  int32_t p, e;

  e = w;  e -= y;

  p = e;  p *= (int32_t)CTRL_PI_K;
  p -= z;
  z = e*(int32_t)CTRL_PI_Kn;

  if (intg > 0) {
    if (p >= CTRL_INT_MAX - intg) {
      intg = CTRL_INT_MAX;
      led_set(LED_YEL, 1);
    }
    else {
      intg += p;
      led_set(LED_YEL, 0);
      led_set(LED_BLU, 0);
    }
  }
  else {
    if (p <= -CTRL_INT_MAX - intg) {
      intg = -CTRL_INT_MAX;
      led_set(LED_BLU, 1);
    }
    else {
      intg += p;
      led_set(LED_YEL, 0);
      led_set(LED_BLU, 0);
    }
  }

  return intg/CTRL_INT_DIV;
}

#if 0
/*** kmitani -- zobrazeni prechodaku ***/
void control(int32_t ad_i, int16_t ad_x) {
  int32_t u;

#define REF_PERIOD 60
  static int counter = 0;
  float w;

  if (counter++ == 2*REF_PERIOD)
    counter = 0;
  w = (counter >= REF_PERIOD) ? (1.0/CTRL_AMP_MUL) : 0.0; //(-1.0/CTRL_AMP_MUL);

  u = pi_ctrl(w, ad_i);

  hbridge_set(u);
  current_negative = (u < 0);

  ((int16_t*)tx_msg.data)[counter%4] = ad_i;
  if (counter%4 == 3)
    can_tx_msg(&tx_msg);
}
#endif

volatile int16_t control_w = 0; /* reference current (*12) */
volatile int32_t control_y; /* measured current (*12*80) */
volatile int32_t control_x; /* measured position (*80) */
volatile int32_t control_u; /* applied PWM (*80) */
volatile int8_t control_on = 0; /* switches H-bridge on/off */

int32_t ctrl_ma_filter(int32_t *mem, uint8_t *idx, int32_t val) {
  int32_t diff = val - mem[*idx];
  mem[*idx] = val;
  if (++*idx == CAN_OVERSAMP)
    *idx = 0;
  return diff;
}

void control(int32_t ad_i, int16_t ad_x) {
  static int32_t cy = 0, cx = 0, cu = 0;
  static int ma_y[CAN_OVERSAMP],
    ma_x[CAN_OVERSAMP], ma_u[CAN_OVERSAMP];
  static uint8_t idx_y = 0, idx_x = 0, idx_u = 0;
  int32_t u;

#if 0
  /* boj s blbym merenim proudu */
#define CTRL_I_MIN 80
  static int8_t sub_pwm = 0;
  int32_t cw, im;
  if (control_w > 0) {
    cw = (control_w + 1)/2;
    im = 2*CTRL_I_MIN;
  }
  else {
    cw = (-control_w - 1)/2;
    im = -2*CTRL_I_MIN;
  }
  if (cw < CTRL_I_MIN)
    u = pi_ctrl((cw > sub_pwm) ? im : 0, ad_i);
  else
    u = pi_ctrl(control_w, ad_i);
  sub_pwm = (sub_pwm + 1) % CTRL_I_MIN;
  /* job */
#else
  u = pi_ctrl(control_w, ad_i);
#endif

  if (control_on)
    hbridge_set(u);
  current_negative = (u < 0);

  cy += ctrl_ma_filter(ma_y, &idx_y, ad_i);
  cx += ctrl_ma_filter(ma_x, &idx_x, ad_x);
  cu += ctrl_ma_filter(ma_u, &idx_u, u);
  /***** pozustak mylne honby za synchronizaci... ****/
  //control_y = 80*ad_i; //cy;
  //control_x = 80*ad_x; //cx;
  //control_u = 80*u; //cu;
  control_y = cy;
  control_x = cx;
  control_u = cu;
}

/*****/

void adc_irq() {
  static uint8_t ma_idx = 0;
  static int32_t ma_val[ADC_MA_LEN];
  static int8_t negative = 0;
  int32_t ad;
  int32_t ma_diff;
  int8_t last;

  /* read & clear irq flag */
  ad = ADDR;

  /* reset MAT0.1 */
  T0EMR = 0x1<<6 | 0x2; /* must be here due to ADC.5 erratum */

  if ((last = (timer_count == ADC_OVERSAMP-1))) {
    /* last sample before PWM period end */
    /* twice -- ADC.2 erratum workaround */
    ADCR = adc_setup(1<<ADCIN_POS, 14, 1, 0x0 /* now! */, 1);
    ADCR = adc_setup(1<<ADCIN_POS, 14, 1, 0x1 /* now! */, 1);
  }

  ad = (ad>>6)&0x3ff;

  /***! odpor na 3.3V !***/
  //static int32_t ad_zero = 5.0/(12*CTRL_AMP_MUL);
  //if (!control_on) ad_zero = ad;
  //ad -= ad_zero;

  /**** boj s kmitanim ****/
  //ad += 24;
  /****/
  if (negative)
    ad = -ad;
  /* shift value through MA filter */
  ma_diff = ad - ma_val[ma_idx];
  ma_val[ma_idx] = ad;
  if (++ma_idx == ADC_MA_LEN)
    ma_idx = 0;
  /* MA filter output (should be atomic): */
  adc_i += ma_diff;

  if (last) {
    while (((ad = ADDR)&0x80000000) == 0);
    adc_x = (ad>>6)&0x3ff;
    /* twice -- ADC.2 erratum workaround */
    ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x0 /*MAT0.1*/, 1);
    ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x4 /*MAT0.1*/, 1);
    timer_count = 0;
    negative = current_negative;
  }
  else
    ++timer_count;

  /* int acknowledge */
  VICVectAddr = 0;
}

/******/

uint8_t DBG_clk_seq = 0;

void can_rx(){

  led_set(LED_YEL, 1);
  switch (rx_msg.id) {
  case CAN_CLOCK_ID:
    if ((uint16_t)rx_cmd_value != CTRL_OFF) {
      control_w = rx_cmd_value;
      control_on = 1;
      DBG_clk_seq = rx_msg.data[0];
    }
    tx_request = 1;
    break;
  case CAN_CFGMSG_ID:
    if (((uint16_t*)rx_msg.data)[0] == MOTOR_ID) {
      cmd_msg_id = ((uint16_t*)rx_msg.data)[1];
      cmd_msg_ndx = ((uint16_t*)rx_msg.data)[2];
    }
    break;
  default:
    if (rx_msg.id == cmd_msg_id) {
      rx_cmd_value = ((int16_t*)rx_msg.data)[cmd_msg_ndx]; //zde si nejsem jisty tito pretypovanim
      if ((uint16_t)rx_cmd_value == CTRL_OFF) {
	control_on = 0;
	hbridge_off();
      }
    }
    break;
  }
  
  led_set(LED_YEL, 0);
}
uint32_t btr = 0;
void can_tx() {

  /* x: 16b, y: 24b, u: 24b */
  uint16_t control_2 = (uint16_t)(control_x/2);
  tx_msg.data[0] = *((uint8_t*)&control_2);
  tx_msg.data[1] = *((uint8_t*)&control_2 + 1);
  tx_msg.data[2] = *((uint8_t*)&control_y);
//  tx_msg.data[2] = *((uint8_t*)&rx_cmd_value+1);
  tx_msg.data[3] = *((uint8_t*)&control_y + 1);
//  tx_msg.data[3] = *((uint8_t*)&rx_cmd_value);
  tx_msg.data[4] = *((uint8_t*)&control_y + 2);
  tx_msg.data[5] = *((uint8_t*)&control_u);
  tx_msg.data[6] = *((uint8_t*)&control_u + 1);
  tx_msg.data[7] = *((uint8_t*)&control_u + 2);
 
  /* do NOT wait */
  vca_send_msg_seq(can,&tx_msg,1);
  tx_request = 0;
}

/*****************/

void sys_pll_init(uint32_t f_cclk, uint32_t f_osc) {
  uint32_t m, p, p_max;

  led_set(LED_YEL, 1);

  /* turn memory acceleration off */
  MAMCR = 0x0;

  /* compute PLL divider and internal osc multiplier */
  m = f_cclk/f_osc;
  p_max = 160000000/f_cclk;
  for (p = 3; ((1<<p) > p_max) && (p > 0); p = p>>1);
  /* switch PLL on */
  PLLCFG = (p<<5) | ((m-1)&0x1f);
  PLLCON = 0x1;
  PLLFEED = 0xaa;  PLLFEED = 0x55;
  /* wait for lock */
  while (!(PLLSTAT & (1<<10)));
  /* connect to clock */
  PLLCON = 0x3;
  PLLFEED = 0xaa;  PLLFEED = 0x55;

  /* turn memory acceleration on */
  MAMTIM = f_cclk/20000000;
  MAMCR = 0x2;

  led_set(LED_YEL, 0);
}

int main() {
  led_init();
  led_set(LED_GRN, 1);
  /* switch the CAN off due to clock change */
  //can_off(0);
  /* boost clock to 60MHz */
  sys_pll_init(60000000, 10000000);
  /* peripheral clock = CPU clock (60MHz) */
  VPBDIV = 1;

  /* init Vector Interrupt Controller */
  VICIntEnClr = 0xFFFFFFFF;
  VICIntSelect = 0x00000000;

  /* CAN bus setup */
  
  /*inicialice CANu*/
  lpcan_btr(&btr, 1000000 /*Bd*/, 60000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
  lpc_vca_open_handle(&can, 0/*device*/, 1/*flags*/, btr, 10, 11, 12);	

  tx_msg.id = MOTOR_ID;

  hbridge_init();
//  hbridge_set((PWM_PERIOD/ADC_OVERSAMP-2*PWM_DEAD));

  set_irq_handler(18 /*ADC*/, 13, adc_irq);
  adc_init();
  timer_init();


  for (;;) {
    if(vca_rec_msg_seq(can,&rx_msg,1)<1){
     if (timer_count == 1)
       control(adc_i, adc_x);
     if (tx_request)
       can_tx();
    }
    else can_rx();
  }
}
