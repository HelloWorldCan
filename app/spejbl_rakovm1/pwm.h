#include <lpc21xx.h>
#include <types.h>

extern uint32_t PWM_PINSEL[];
extern uint32_t *PWM_MR[];

void pwm_chanel(int n, int double_edge);
void pwm_set(int n, uint32_t when);
void pwm_set_double(int n, uint32_t from, uint32_t to);
void pwm_init(uint32_t prescale, uint32_t period);
