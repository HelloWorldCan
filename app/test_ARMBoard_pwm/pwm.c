/*
*  C Implementation: pwm
*
* Description: configuration of PWM unit
*
*
* Author: LAGARRIGUE <glagarri@etud.insa-toulouse.fr>, (C) 2005
*
* Copyright: See COPYING file that comes with this distribution
*
*/


#include <LPC210x.h>
#include "config.h"
#include "pwm.h"

#define MINFREQ  (PCLK/(0xFFFFFFFF))

/**
 * Initializes PWM frequency with match0 register
 * @param freq   frequency in Hz
 */
void Init_PWM(int freq)
{
    if ((freq <= PCLK) | (freq >= MINFREQ))
    { 
        PWMPR  = 0x00000000;    //no prescaler
        PWMMCR = 0x00000002;    // on match with MATCH0 Register, reset the counter
        PWMMR0 = (int)(PCLK/(freq));
    }
 
              
} 

/**
 * Initilizes and sets the duty cycle of the desired pwm channel
 * @param channel   = channel number (1,2,3,4,5,6)
 * @param duty_cycle  = float between 0 and 1
 */
void Set_PWM (int channel, float duty_cycle)
{
        int Pwmmatch;
        Pwmmatch = (int) (duty_cycle * PWMMR0);
        
        
        
        //PINSEL0 |= ...;                    /* Enable P0.7, 8 and P0.9 as alternate PWM outputs functions*/
        //PINSEL0 &= ...;
          
        //PWMPCR = ...;                      /* single edge control only*/
        
        //PWMMRX = 0x500;                           /* set falling edge of PWM channel X       */
                                
        //PWMLER = 0x..;                             /* enable shadow latch for match 1 - 6   */ 
       
        
        switch (channel)
    {
        
    
        case 1 :
            PWMMR1 =Pwmmatch;
                PWMPCR |= 0x0200; 
                PWMLER |= 0x02;
                PINSEL0 |= 0x00000002;                    
                PINSEL0 &= 0xFFFFFFFE;
                break;
        
        case 2 :
                PWMMR2 =Pwmmatch; 
                PWMPCR |= 0x0400; 
                PWMLER |= 0x04;
                PINSEL0 |= 0x00008000;                    
                PINSEL0 &= 0xFFFFBFFF;
                break;
        
        case 3 :
            PWMMR3 =Pwmmatch;
                PWMPCR |= 0x0800; 
                PWMLER |= 0x08;
                PINSEL0 |= 0x00000008;                    
                PINSEL0 &= 0xFFFFFFFB;
                break;
        case 4 : 
            PWMMR4 =Pwmmatch; 
                PWMPCR |= 0x1000; 
                PWMLER |= 0x10;
                PINSEL0 |= 0x00020000;                    
                PINSEL0 &= 0xFFFEFFFF;
                break;
                    
        case 5 :
            PWMMR5 =Pwmmatch; 
                PWMPCR |= 0x2000; 
                PWMLER |= 0x20;
                PINSEL1 |= 0x00000400;                    
                PINSEL1 &= 0xFFFFF7FF;
                break;
                    
        case 6 :
                PWMMR6 =Pwmmatch; 
                PWMPCR |= 0x4000; 
                PWMLER |= 0x40;
                PINSEL0 |= 0x00080000;                    
                PINSEL0 &= 0xFFFBFFFF;
                break;
    
    
    }

} 


/**
 * Resets the counter and runs the PWM unit
 * @param  
 */
void Run_PWM (void)
{
    PWMTCR = 0x2;                      /* Reset counter and prescaler           */ 
    PWMTCR = 0x9;                      /* enable counter and PWM, release counter from reset */
}


/**
 * Stops PWM unit and resets the timer
 * @param  
 */
void Stop_PWM (void)
{
    int i;
    for (i=1 ; i < 7 ; i++)
        {
            Set_PWM (i, 0);            
        }
    PWMTCR = 0x2;
}