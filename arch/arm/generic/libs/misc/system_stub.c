/* Support files for GNU libc.  Files in the system namespace go here.
   Files in the C namespace (ie those that do not start with an
   underscore) go in .c.  */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <errno.h>
#include <reent.h>
#include <system_def.h>

/* Register name faking - works in collusion with the linker.  */
register char * stack_ptr asm ("sp");

system_stub_ops_t system_stub_ops;

int
_read (int file,
       char * ptr,
       int len)
{
  if(!system_stub_ops.read)
    return -1;
  return system_stub_ops.read(file,ptr,len);
}

int
_write (int    file,
	char * ptr,
	int    len)
{
  if(!system_stub_ops.write)
    return len;
  return system_stub_ops.write(file,ptr,len);
}


int
_lseek (int file,
	int pos,
	int dir)
{
  if(!system_stub_ops.lseek)
    return -1;
  return system_stub_ops.lseek(file,pos,dir);
}

int
_open (const char * path,
       int          flags,
       ...)
{
  if(!system_stub_ops.open)
    return -1;
  return system_stub_ops.open(path,flags,0);
}

int
_close (int file)
{
  if(!system_stub_ops.close)
    return -1;
  return system_stub_ops.close(file);
}

typedef int (local_call_t)(void);

void
_exit (int n)
{
  ((local_call_t*)0)();
  while(1);
}

int
_kill (int n, int m)
{
  return -1;
}

int
_getpid (int n)
{
  return 1;
}


caddr_t
_sbrk (int incr)
{
  extern char   end asm ("end");	/* Defined by the linker.  */
  static char   *heap_end;
  char *        prev_heap_end;

  incr=(incr+3) & ~3;

  if (heap_end == NULL)
    heap_end = & end;
  
  prev_heap_end = heap_end;
    
  if (heap_end + incr > stack_ptr)
    {
      /* Some of the libstdc++-v3 tests rely upon detecting
	 out of memory errors, so do not abort here.  */
#if 0
      extern void abort (void);

      _write (1, "_sbrk: Heap and stack collision\n", 32);
      
      abort ();
#else
      errno = ENOMEM;
      return (caddr_t) -1;
#endif
    }
  
  heap_end += incr;

  return (caddr_t) prev_heap_end;
}

int
_fstat (int file, struct stat * st)
{
  return -1;
}

int _stat (const char *fname, struct stat *st)
{
  return -1;
}

int
_link (void)
{
  return -1;
}

int
_unlink (void)
{
  return -1;
}

void
_raise (void)
{
  return;
}

int
_gettimeofday (struct timeval * tp, struct timezone * tzp)
{

  if(tp) 
    {
      tp->tv_sec = 0;
      tp->tv_usec = 0;
    }
  /* Return fixed data for the timezone.  */
  if (tzp)
    {
      tzp->tz_minuteswest = 0;
      tzp->tz_dsttime = 0;
    }

  return 0;
}

/* Return a clock that ticks at 100Hz.  */
clock_t 
_times (struct tms * tp)
{
  clock_t timeval = 0;
  
  if (tp)
    {
      tp->tms_utime  = timeval;	/* user time */
      tp->tms_stime  = 0;	/* system time */
      tp->tms_cutime = 0;	/* user time, children */
      tp->tms_cstime = 0;	/* system time, children */
    }
  
  return timeval;
};


int
isatty (int fd)
{
  return 1;
}

int
_system (const char *s)
{
  return -1;
}

int
_rename (const char * oldpath, const char * newpath)
{
  return -1;
}
