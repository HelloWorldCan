#ifndef _SYSTEM_STUB_H_
#define _SYSTEM_STUB_H_

#include <types.h>

typedef struct system_stub_ops_t {
  int (*open)(const char * path, int flags, ...);
  int (*close)(int file);
  int (*read)(int file, char * ptr, int len);
  int (*write)(int file, char * ptr, int len);
  int (*lseek)(int file, int ptr, int dir);
} system_stub_ops_t;

extern system_stub_ops_t system_stub_ops;

#endif /* _SYSTEM_DEF_H_ */
