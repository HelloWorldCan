/**************************** lpc_sys.h *********************************/
/* Copyright 2003/12/29 Aeolus Development				*/
/* All rights reserved.							*/
/*									*/
/* Redistribution and use in source and binary forms, with or without	*/
/* modification, are permitted provided that the following conditions	*/
/* are met:								*/
/* 1. Redistributions of source code must retain the above copyright	*/
/*   notice, this list of conditions and the following disclaimer.	*/
/* 2. Redistributions in binary form must reproduce the above copyright	*/
/*   notice, this list of conditions and the following disclaimer in the*/
/*   documentation and/or other materials provided with the 		*/
/*   distribution.							*/
/* 3. The name of the Aeolus Development or its contributors may not be	*/
/* used to endorse or promote products derived from this software 	*/
/* without specific prior written permission.				*/
/*									*/
/* THIS SOFTWARE IS PROVIDED BY THE AEOULUS DEVELOPMENT "AS IS" AND ANY	*/
/* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE	*/
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR	*/
/* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AEOLUS DEVELOPMENT BE	*/
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR	*/
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF	*/
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	*/
/* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,*/
/* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE */
/* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 	*/
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.			*/
/*									*/
/*  System control.  Basic support that doesn't really fit as part of a */
/* device driver.							*/
/************************************************************************/
/*
*   TLIB revision history:
*   1 lpc_sys.h 30-Dec-2003,10:34:12,`RADSETT' First archival version.
*   2 lpc_sys.h 17-Jan-2004,16:04:06,`RADSETT' Correct title line.
*   3 lpc_sys.h 29-Jan-2004,11:28:14,`RADSETT' Update Prototypes.
*        Add MinimumAchievableWait prototype.
*        Spelling corrections
*   TLIB revision history ends.
*/
/*lint -library*/
#ifndef LPC_SYS__H
#define LPC_SYS__H

#include <reent.h>

	/**** Structures & enums. ****/

typedef enum {		/*  Enumerates the possible VPB clock dividers	*/
    VPB_DIV1,		/*  VPB clock = CPU clock			*/
    VPB_DIV2,		/*  VPB clock = CPU clock / 2			*/
    VPB_DIV4		/*  VPB clock = CPU clock / 4			*/
    } VPB_param;

typedef enum {		/*  Enumerates MAM modes.			*/
    MAM_disabled,	/*  MAM disabled.				*/
    MAM_part_enable,	/*  MAM partially enabled.			*/
    MAM_full_enable	/*  MAM fully enabled.				*/
    } MAM_CONTROL;

/********************* ActualSpeed **************************************/
/*  ActualSpeed -- Returns the operating speed of the CPU.  Relies on 	*/
/* earlier call to set native oscillator speed correctly.		*/
unsigned long ActualSpeed( void);

/********************* GetUs ********************************************/
/*  GetUs -- Get the current time in uS.				*/
unsigned long long GetUs( void);

/********************* MinimumAchievableWait ****************************/
/*  MinimumAchievableWait -- Get the shotest wait we can do.		*/
unsigned int MinimumAchievableWait(void);

/********************* SetDesiredSpeed **********************************/
/*  SetDesiredSpeed -- Set the cpu to desired frequency.  Relies on 	*/
/* earlier call to set native oscillator speed correctly.  Returns 0	*/
/* if successful.  desired_speed is set to actual speed obtained on 	*/
/* return.								*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  unsigned long desired_speed	-- CPU operating frequency in kHz.	*/
/*  Returns 0 if successful.  errno will be set on error.		*/
int SetDesiredSpeed( struct _reent *r, unsigned long desired_speed);

/********************* SetMAM *******************************************/
/*  SetMAM -- Set up the MAM.  Minimal error checking, not much more 	*/
/* than a wrapper around the register.  Returns 0 if successful, 	*/
/* something else.  Sets errno in case of an error.			*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  unsigned int cycle_time -- number of cycles to access the flash.	*/
/*  MAM_CONTROL ctrl	-- Mode to place MAM in.  One of:		*/
/*				MAM_disabled, MAM_part_enable, or 	*/
/*				MAM_full_enable.			*/
/*  Returns 0 if successful. Additional error/sanity checks are possible*/
int SetMAM( struct _reent *r, unsigned int cycle_time, MAM_CONTROL ctrl);

/********************* SetNativeSpeed ***********************************/
/*  SetNativeSpeed -- Set the oscillator frequency for the external 	*/
/* oscillator.  This is used to inform the routines that deal with cpu	*/
/* frequencies what the starting point is.  Any error here will be 	*/
/* multiplied later.							*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  unsigned long speed	-- external oscillator/crystal frequency in kHz.*/
/*  Note:  There is no way to determine or verify this value so we have	*/
/* to trust the caller to get it right.					*/
/*  Returns 0 if successful.						*/
int SetNativeSpeed( struct _reent *r, unsigned long speed);

/********************* StartClock ***************************************/
/*  StartClock -- Starts up the clock used for internal timing.  	*/
/* Attempts to match the desired clock speed (CLOCK_SPEED) and 		*/
/* initializes timing_scale_factor to a compensating scale.  Returns 0	*/
/* if successful, otherwise error code will be retained in errno.	*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  Note:  Should be called only after all clocks have been set up.	*/
/* Otherwise time scale will not be correct.				*/
/*  Returns 0 if successful.						*/
int StartClock( struct _reent *r);

/********************* VPBControl ***************************************/
/*  VPBControl -- Control the clock divider on the peripheral bus.	*/
/* Returns the actual divider in ptr (requested is also passed in ptr.	*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  VPB_parm p		-- requested VPB to CPU freq rate.		*/
/*  Returns 0 if successful.						*/
int VPBControl( struct _reent *r, VPB_param p);

/********************* VPBRate ******************************************/
/*  VPBRate -- Finds and returns the rate of the clock on the 		*/
/* peripheral bus (in Hz).  						*/
unsigned long VPBRate( void);

/********************* UsToCounts ***************************************/
/*  UsToCounts -- converts to internal units in counts from uS.  Other 	*/
/* Modules use this counter for a timebase so this needs to be 		*/
/* available to them.							*/
/*  unsigned int us	-- microseconds to convert to counts.	 	*/
/*  Returns number of counts corresponding to us.  Saturates on		*/
/* overflow so for large time periods it is possible to get a result	*/
/* lower than requested.						*/
unsigned int UsToCounts( unsigned int us);

/********************* WaitUs *******************************************/
/*  WaitUs -- Wait for 'wait_time' us					*/
/*  unsigned int wait_time	-- microseconds to convert to counts. 	*/
/*  Will break wait into multiple waits if needed to avoid saturation.	*/
void WaitUs( unsigned int wait_time);

#endif /* LPC_SYS__H */
