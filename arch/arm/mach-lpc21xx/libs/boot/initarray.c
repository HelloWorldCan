/*
 * Copyright (C) 2004 CodeSourcery, LLC
 *
 * Permission to use, copy, modify, and distribute this file
 * for any purpose is hereby granted without fee, provided that
 * the above copyright notice and this notice appears in all
 * copies.
 *
 * This file is distributed WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
#include <sys/types.h>

/* Handle ELF .init_array sections.  */

/* These magic symbols are provided by the linker.  */
extern void (*__init_array_start []) (void) __attribute__((weak));
extern void (*__init_array_end []) (void) __attribute__((weak));

/* /\* Iterate over all the init routines.  *\/ */
/* void */
/* __libc_init_array (void) */
/* { */
/*   void (*func)(void); */

/*   for (func = (void (*)(void))__init_array_start; */
/*        func < (void (*)(void))__init_array_end; */
/*        func++) { */
/*     (*func)(); */
/*   } */
/* } */

/* Iterate over all the init routines.  */
void
__libc_init_array (void)
{
  size_t count;
  size_t i;

  count = __init_array_end - __init_array_start;
  for (i = 0; i < count; i++)
    __init_array_start[i] ();
}
