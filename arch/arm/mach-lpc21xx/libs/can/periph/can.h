#include <stdlib.h>
#include <types.h>
#include <lpc21xx.h>

typedef struct _can_msg_t {
#define CAN_MSG_RTR    0x40000000
#define CAN_MSG_EXTID  0x80000000
  uint32_t flags;
  unsigned short dlc;
  uint32_t id;
  unsigned char data[8];
} can_msg_t;

typedef void (*can_rx_callback)(can_msg_t *msg);

/* private global variables */
can_rx_callback can_rx_cb;

/* public global variables */
extern volatile int can_msg_received;
extern volatile can_msg_t can_rx_msg;

/* function prototypes */
void can_init(uint32_t btr, unsigned rx_isr_vect, can_rx_callback rx_cb); // Marek Peca function btr is register	

void can_init_baudrate(uint32_t baudrate, unsigned rx_isr_vect, can_rx_callback rx_cb); // Jiri Kubias function  auto baudrate calculation
void can_off();
int can_tx_msg(can_msg_t *tx_msg);
int can_self_tx_msg(can_msg_t *tx_msg);

void can_rx_isr();

/*EOF*/
