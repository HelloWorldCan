/*
 * ON-TRACK CAN driver library for Philips LPC2xxx microcontroller family
 * Marek Peca <mp@duch.cz>, 2008/04
 *
 * Driver works around all that known severe hardware bugs of first LPC2xxx
 * series (ie., without /00 or /01 suffix).
 */

#include <cpu_def.h>
#include <string.h>
#include "canmsg.h"
#include "lpcan.h"

#define ERRATUM_CAN_5
#define ERRATUM_CAN_7

#define CAN_RX_BUF_LEN 8
#define CAN_TX_BUF_LEN 8

#define INVALID_ID     0xffffffff
#define CAN_MSG_RTR    0x40000000
#define CAN_MSG_EXT    0x80000000

#define reg(device,reg1) (*((uint32_t*)((void*)&(reg1) + (device<<14))))
#define valid(device) ((device >= 0) && (device < CAN_NUM_DEVICES))
#define running(device) (can_dev_on & (1<<device))

typedef struct {
  canmsg_t *buf;
  int length;
  int get_idx, put_idx, full;
} cyclbuf_t;

static canmsg_t
  rx_buf[CAN_NUM_DEVICES][CAN_RX_BUF_LEN],
  tx_buf[CAN_NUM_DEVICES][CAN_TX_BUF_LEN];

volatile static struct can_dev_t {
  can_callback cb;
  int cb_event_mask;
  cyclbuf_t rx_buf, tx_buf;
  unsigned error;
  uint32_t pending_tx_id[2];
  int pending_idx;
} can_dev[CAN_NUM_DEVICES];

volatile static unsigned can_dev_on = 0;

void can_rx_isr() __attribute__((interrupt));
void can_tx_isr() __attribute__((interrupt));
void can_err_isr() __attribute__((interrupt));

inline void cyclbuf_init(cyclbuf_t *c) {
  c->get_idx = c->put_idx = c->full = 0;
}

inline int cyclbuf_empty(cyclbuf_t *c) {
  return (!(c->full) && (c->put_idx == c->get_idx));
}

inline int cyclbuf_put(cyclbuf_t *c, canmsg_t *msg) {
  unsigned long flags;

  if (c->full && (c->put_idx == c->get_idx))
    /* overflow */
    return -1;

  memcpy(&c->buf[c->put_idx], msg, sizeof(canmsg_t));
  save_and_cli(flags);
  c->put_idx = (c->put_idx+1) % c->length;
  c->full = (c->put_idx == c->get_idx);
  restore_flags(flags);
  return 0;
}

inline int cyclbuf_get(cyclbuf_t *c, canmsg_t *msg) {
  unsigned long flags;

  if (cyclbuf_empty(c))
    /* underflow */
    return -1;

  memcpy(msg, &c->buf[c->get_idx], sizeof(canmsg_t));
  save_and_cli(flags);
  c->get_idx = (c->get_idx+1) % c->length;
  c->full = 0;
  restore_flags(flags);
  return 0;
}

/**
 * @param device CAN controller number (0, 1,...)
 * @param btr bit timing register (BTR) value, see LPC2xxx docs; can be set by <code>lpcan_btr()</code> function
 * @param rx_ivect receive interrupt vector number
 * @param tx_ivect transmit interrupt vector number
 * @param err_ivect error interrupt vector number
 * @param cb user supplied event callback function or NULL if none
 * @param cb_event_mask bitwise or of CAN_RX, CAN_TX or CAN_ERR events or 0 if not interested
 *
 * Perform controller reset and (re)initialization.
 * Interrupt vector numbers (see LPC2xxx VIC documentation) must be supplied
 * by the caller in order to avoid conflicts with rest of hardware.
 * According to CAN.5 erratum workaround, a dummy message with id=0x0 may be
 * transmitted after initialization.
 */
void can_init(int device, uint32_t btr,
	      unsigned rx_ivect, unsigned tx_ivect, unsigned err_ivect,
	      can_callback cb, int cb_event_mask)
{
  if (!valid(device) || running(device))
    return;

  /* enable CAN Rx (Tx) pins */
  switch (device) {
  case 0:
    /* RD1 */
    PINSEL1 = (PINSEL1 | (1<<18)) & ~(1<<19);
    break;
  case 1:
    /* RD2, TD2 */
    PINSEL1 = (PINSEL1 | (1<<14)) & ~(1<<15);
    PINSEL1 = (PINSEL1 | (1<<16)) & ~(1<<17);
    break;
  }

  /* init persistent data structure */
  can_dev_on |= (1<<device);
  can_dev[device].cb = cb;
  can_dev[device].cb_event_mask = (cb == NULL) ? 0 : cb_event_mask;
  /* static pointers, may be later replaced by malloc() */
  cyclbuf_init((cyclbuf_t*)&can_dev[device].rx_buf);
  can_dev[device].rx_buf.buf = rx_buf[device];
  can_dev[device].rx_buf.length = CAN_RX_BUF_LEN;
  cyclbuf_init((cyclbuf_t*)&can_dev[device].tx_buf);
  can_dev[device].tx_buf.buf = tx_buf[device];
  can_dev[device].tx_buf.length = CAN_TX_BUF_LEN;
  /* reset error flags */
  can_dev[device].error = 0;
  /* reset pending Tx ID list */
  can_dev[device].pending_idx = 0;
  can_dev[device].pending_tx_id[0] = INVALID_ID;
  can_dev[device].pending_tx_id[1] = INVALID_ID;

  /* receive all messages, no filtering */
  AFMR = 0x2;
  /* reset mode */
  reg(device, C1MOD) = 0x1;
  /* -- addition from lpc2000 maillist msg #3052: */
  reg(device, C1CMR) = 0x0e; /* Clear receive buffer, data overrun, abort tx */
  /* -- end of addition */
  reg(device, C1IER) = 0x0;
  reg(device, C1GSR) = 0x0;
  /* set baudrate & timing */
  reg(device, C1BTR) = btr;
  /* set interrupt vectors */
  /* Rx & Tx per device */
  ((uint32_t*)&VICVectAddr0)[rx_ivect] = (uint32_t)can_rx_isr;
  ((uint32_t*)&VICVectCntl0)[rx_ivect] = 0x20 | (26 + device);
  ((uint32_t*)&VICVectAddr0)[tx_ivect] = (uint32_t)can_tx_isr;
  ((uint32_t*)&VICVectCntl0)[tx_ivect] = 0x20 | (20 + device);
  /* err ("other") is global */
  ((uint32_t*)&VICVectAddr0)[err_ivect] = (uint32_t)can_err_isr;
  ((uint32_t*)&VICVectCntl0)[err_ivect] = 0x20 | 19;
  /* enable interrupts */
  VICIntEnable = (1<<(26 + device)) | (1<<(20 + device)) | (1<<19);
  /* RIE | TIE1 | EIE | DOIE | EPIE | ALIE | BEIE, ignore WUIE, IDIE, TIE2,3 */
  reg(device, C1IER) = 0xef;
  /***DEBUG!!!***/
  reg(device, C1EWL) = 0xff;
  /**************/
  /* normal (operating) mode */
  reg(device, C1MOD) = 0x0;

#ifdef ERRATUM_CAN_5
  /* LPC2119 CAN.5 erratum workaround --!!! transmit dummy msg with id=0x000 */
  reg(device, C1TFI1) = 0x00000000;
  reg(device, C1TID1) = 0x0;
  reg(device, C1CMR) = 0x23;
#endif
}

/**
 * @param device CAN controller number
 *
 * Disable and reset the particular controller.
 */
void can_off(int device) {
  if (!valid(device) || !running(device))
    return;

  can_dev_on &= ~(1<<device);
  /* disable interrupts */
  VICIntEnClr = (1<<(26 + device)) | (1<<(20 + device)) |
    ((can_dev_on == 0) ? (1<<19) : 0);
  reg(device, C1IER) = 0x0;
  /* abort transmission */
  reg(device, C1CMR) = 0x2;
  /* switch off the CAN controller */
  reg(device, C1MOD) = 0x1;
}

/**
 * @param device CAN controller number
 * @param msg message pointer
 * @return 0 if message read, -1 if no message available or error occured
 *
 * Read a message from CAN, if any.
 * Non-blocking call.
 */
int can_rx_msg(int device, canmsg_t *msg) {
  if (!valid(device)) /* allow reading of pending msgs after can_off() */
    return -1;
  return cyclbuf_get((cyclbuf_t*)&can_dev[device].rx_buf, msg);
}

int can_tx_try(int device) {
  uint32_t data, flags, id;
  canmsg_t msg;

  if ((reg(device, C1SR) & 0x4) == 0)
    /* Tx1 transmission pending */
    return -1;

  cyclbuf_get((cyclbuf_t*)&can_dev[device].tx_buf, &msg);
  /* fill Tx1 registers */
  flags = (msg.flags & MSG_RTR) ? CAN_MSG_RTR : 0;
  if (msg.flags & MSG_EXT) {
    flags |= CAN_MSG_EXT;
    id = msg.id & ((1<<29)-1);
  }
  else
    id = msg.id & ((1<<11)-1);
  reg(device, C1TFI1) = flags | ((msg.length<<16) & 0x000f0000);
  reg(device, C1TID1) = id;

  if (msg.length > 0) {
    memcpy(&data, msg.data, 4);
    reg(device, C1TDA1) = data;
    if (msg.length > 4) {
      memcpy(&data, &msg.data[4], 4);
      reg(device, C1TDB1) = data;
    }
  }

  /* start transmission */
#ifdef ERRATUM_CAN_7
  /*
    use Self Reception Req instead of Transmission Req
    according to CAN.7 workaround;
    this will cause reception of self-transmitted msg,
    which should be ignored by the receiving user software
  */
  reg(device, C1CMR) = 0x30;
  /* add the ID to the list of pending Tx ID's */
  can_dev[device].pending_tx_id[can_dev[device].pending_idx] = id;
  can_dev[device].pending_idx ^= 1;
#else
  reg(device, C1CMR) = 0x21;
#endif
  return 0;
}

/**
 * @param device CAN controller number
 * @param msg message pointer
 * @return 0 if message sent, -1 if transmit buffer overflow or error occured
 *
 * Transmit a message to CAN.
 * Non-blocking call.
 */
int can_tx_msg(int device, canmsg_t *msg) {
  unsigned long flags;

  if (!valid(device) || !running(device))
    return -1;
  if (cyclbuf_put((cyclbuf_t*)&can_dev[device].tx_buf, msg))
    /* overflow */
    return -1;

  save_and_cli(flags);
  can_tx_try(device);
  restore_flags(flags);
  return 0;
}

void can_rx_isr() {
  int device, event;
  uint32_t flags;
  canmsg_t msg;

  for (device = 0; device < CAN_NUM_DEVICES; device++) {
    if (!running(device))
      continue;
    if (!(reg(device, C1GSR) & 0x1))
      /* Rx msg @ this device? */
      continue;

    msg.id = reg(device, C1RID);
#ifdef ERRATUM_CAN_7
    int i = can_dev[device].pending_idx, blacklisted = 0;

    if (msg.id == can_dev[device].pending_tx_id[i]) {
      can_dev[device].pending_tx_id[i] = INVALID_ID;
      blacklisted = 1;
    }
    else if (msg.id == can_dev[device].pending_tx_id[i^1]) {
      can_dev[device].pending_tx_id[i^1] = INVALID_ID;
      blacklisted = 1;
    }
    if (blacklisted) {
      /* release Rx buffer */
      reg(device, C1CMR) = 0x4;
      continue;
    }
#endif
    flags = reg(device, C1RFS);
    msg.flags = (flags & CAN_MSG_RTR) ? MSG_RTR : 0;
    if (flags & CAN_MSG_EXT)
      msg.flags |= MSG_EXT;
    msg.length = (flags>>16) & 0xf;
    if (msg.length > 0) {
      memcpy(msg.data, &reg(device, C1RDA), 4);
      if (msg.length > 4)
	memcpy(&msg.data[4], &reg(device, C1RDB), 4);
    }
    if (cyclbuf_put((cyclbuf_t*)&can_dev[device].rx_buf, &msg)) {
      /* overflow */
      can_dev[device].error |= CAN_ERR_RXOVER;
      event = CAN_ERR;
    }
    else
      /* Rx msg buffered */
      event = CAN_RX;
    if (can_dev[device].cb_event_mask & event)
      can_dev[device].cb(device, event);
    /* release Rx buffer */
    reg(device, C1CMR) = 0x4;
  }

  /* int acknowledge */
  VICVectAddr = 0;
}

void can_tx_isr() {
  int device;

  for (device = 0; device < CAN_NUM_DEVICES; device++) {
    if (!running(device))
      continue;
    if (!(reg(device, C1ICR) & 0x2))
      /* Tx1 buffer freed @ this device? */
      continue;

    /* transmit, if there is anything to send */
    if (!cyclbuf_empty((cyclbuf_t*)&can_dev[device].tx_buf))
      can_tx_try(device);
    /* notify about previous completed Tx */
    if (can_dev[device].cb_event_mask & CAN_TX)
      can_dev[device].cb(device, CAN_TX);
  }

  /* int acknowledge */
  VICVectAddr = 0;
}

void can_err_isr() {
  int device;
  unsigned icr, gsr;

  for (device = 0; device < CAN_NUM_DEVICES; device++) {
    if (!running(device))
      continue;

    icr = reg(device, C1ICR);
    if (icr) {
      gsr = reg(device, C1GSR);
      if (gsr & (1<<1)) {
	/* Data Overrun Status */
	can_dev[device].error |= CAN_ERR_RXOVER;
	/* clear DOS */
	reg(device, C1CMR) = (1<<3);
      }

      if (gsr & (1<<6))
	can_dev[device].error |= CAN_ERR_ERRSTATUS;
      else
	can_dev[device].error &= ~CAN_ERR_ERRSTATUS;

      if (gsr & (1<<7))
	can_dev[device].error |= CAN_ERR_BUSOFF;
      else
	can_dev[device].error &= ~CAN_ERR_BUSOFF;

      if (icr & (1<<7))
	can_dev[device].error |=
	  ((icr & (1<<21)) ? CAN_ERR_RXBUSERR : CAN_ERR_TXBUSERR);
      else
	can_dev[device].error &= ~(CAN_ERR_RXBUSERR | CAN_ERR_TXBUSERR);

      /* error notification */
      if (can_dev[device].cb_event_mask & CAN_ERR)
	can_dev[device].cb(device, CAN_ERR);
    }
  }

  /* int acknowledge */
  VICVectAddr = 0;
}

/**
 * @param device CAN controller number
 * @return bitwise or of error flags, see lpcan.h or 0 if none
 *
 * Report errors for particular controller.
 * Accumulated error occurences are returned, then the internal error
 * accumulator is reset to 0.
 */
unsigned can_error(int device) {
  unsigned long flags;
  unsigned e;

  if (!valid(device))
    return 0;

  save_and_cli(flags);
  e = can_dev[device].error;
  can_dev[device].error = 0;
  restore_flags(flags);
  return e;
}

/**
 * @param device CAN controller number
 * @return 1 if receive buffer is empty, 0 otherwise or -1 if device out of range
 *
 * Test, whether there are incoming messages in receive buffer.
 */
int can_rx_empty(int device) {
  if (!valid(device))
    return -1;
  return cyclbuf_empty((cyclbuf_t*)&can_dev[device].rx_buf);
}

/**
 * @param device CAN controller number
 * @return 1 if transmit buffer is empty, 0 otherwise or -1 if device out of range
 *
 * Test, whether there are any outcoming messages in transmit buffer.
 */
int can_tx_empty(int device) {
  if (!valid(device))
    return -1;
  return cyclbuf_empty((cyclbuf_t*)&can_dev[device].tx_buf);
}

/*EOF*/
