#include <types.h>

void sys_pll_init(uint32_t f_cclk, uint32_t f_osc);
void sys_pll_off();
