#include <lpc21xx.h>
#include "pll.h"

/**
 * Switch LPC2xxx PLL off.
 */
void sys_pll_off() {
  /* shut down PLL */
  PLLCON = 0;
  PLLFEED = 0xaa;  PLLFEED = 0x55;
}
