#include <cpu_def.h>
#include "pwm.h"

void sync_pwm_timer(uint32_t *tc_addr) {
  cli();
  asm volatile
    (
     "mov  r2, %0       \n\t"
     "mov  r3, %1       \n\t"
     "ldr  r1, [r2]     \n\t"
     "add  r1, r1, #12  \n\t"
     "str  r1, [r3]     \n\t"
     : /* no output */ : "r" (tc_addr), "r" (&PWMTC)
     );
  sti();
}
