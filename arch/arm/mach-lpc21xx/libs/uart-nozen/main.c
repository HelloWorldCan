

#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <errno.h>
#include <periph/can.h>
#include <system_def.h>
#include <string.h>
#include <deb_led.h>
#include "uart_nozen.h"




#define	CAN_SPEED 	1000000

#define CAN_ISR		0
#define SERVO_ISR	1
#define UART0_ISR	3
#define UART1_ISR	4	



void dummy_wait(void)
{
    int i = 1000000;
    while(--i);
}



void init_perip(void)	   // inicializace periferii mikroprocesoru
{
	UART_init( UART0,UART0_ISR, 19200, UART_BITS_8, UART_STOP_BIT_1,  UART_PARIT_OFF, 0);
	UART_init( UART1,UART1_ISR, 115100, UART_BITS_8, UART_STOP_BIT_1,  UART_PARIT_OFF, 0);
}



int main (void)  
{

// sample use
	init_perip();			// sys init MCU
 



	while(1)
	{
		while (UART_new_data(UART1))
 		{
 			write_UART_data( UART0, read_UART_data(UART1));
 		}
		
		while (UART_new_data(UART0))
 		{
 			write_UART_data( UART1, read_UART_data(UART0));
 		}
		__deb_led_on(LEDG);
		dummy_wait();
		__deb_led_off(LEDG);
		__deb_led_off(LEDY);	
		dummy_wait();
	} 
}



