/********************************************************/
/*	ZEN UART library for LPC ARM			*/
/*							*/
/*		ver. 3.3 release			*/
/*							*/
/* (c) 2006-2009 Ondrej Spinka, DCE FEE CTU Prague	*/
/*							*/
/* no animals were harmed during development/testing 	*/
/* of this software product, except the author himself	*/
/*							*/
/* you may use this library for whatever purpose you 	*/
/* like, safe for satanistic rituals.			*/
/********************************************************/

#ifndef _UART_ZEN_H
#define _UART_ZEN_H

#include <types.h>
#include <lpc21xx.h>

/*! @defgroup defines Constants Definitions
* @{
*/

/*!\def UART_BUFF_LEN
* Incomming data buffer length.
*/
#define UART_BUFF_LEN 250
/*! @} */

/*!\def UART1_OFFSET
* UART1 address offset.
*/
#define UART1_OFFSET 0x4000

/*!\def UART0
* UART0 number.
*/
#define UART0 0

/*!\def UART1
* UART1 number.
*/
#define UART1 1

/*! @defgroup error_codes Error Codes
* @ingroup defines
* Following constants define the error code masks.
* @{
*/
#define ROUND_BUFFER_OVERRUN_ERR_MASK 0x01
#define RX_BUFFER_OVERRUN_ERR_MASK 0x02
#define PARITY_ERR_MASK 0x04
#define FRAME_ERR_MASK 0x08
#define BREAK_IRQ_ERR_MASK 0x10
/*! @} */
/*! @} */

/*! @defgroup prototypes Function Prototypes
* @{
*/

/*! UART initialization function.
* Takes three arguments:
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \param baud_rate unsigned 32-bit int baud rate in bps
* \param pclk unsigned 32-bit int peripheral clock frequency in Hz
* \param rx_isr_vect unsigned 32-bit int interrupt vector number
*/
void UART_init ( uint8_t uart_num, uint32_t baud_rate, uint32_t pclk, unsigned rx_isr_vect );

/*! Data read function.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns readed character (unsigned 8-bit int).
*/
uint8_t read_UART_data ( uint8_t uart_num );

/*! Determine possible UART transmission errors.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns error code (0 no errors; bit0 - incoming data buffer overrun, bit1 - overrun error, bit2 - parity error, bit3 - framing error bit4 - break interrupt).
*/
uint8_t UART_test_err ( uint8_t uart_num );

/*! Determine whether new data wait in the UART round buffer.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns 1 if there are unreaded data in the incoming data buffer, 0 otherwise.
*/
uint8_t UART_new_data ( uint8_t uart_num );

/*! Data write function.
* Takes two arguments:
* \param uart_num unsigned 8-bit UART number (0 or 1)
* \param data unsigned 8-bit int byte (character) to send
* \return Returns -1 if line was busy and no data were sent, 0 in case of success.
*/
int8_t write_UART_data ( uint8_t uart_num, uint8_t data );
/*! @} */

#endif