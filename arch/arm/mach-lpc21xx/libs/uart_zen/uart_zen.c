/********************************************************/
/*	ZEN UART library for LPC ARM			*/
/*							*/
/*		ver. 3.3 release			*/
/*							*/
/* (c) 2006-2009 Ondrej Spinka, DCE FEE CTU Prague	*/
/*							*/
/* no animals were harmed during development/testing 	*/
/* of this software product, except the author himself	*/
/*							*/
/* you may use this library for whatever purpose you 	*/
/* like, safe for satanistic rituals.			*/
/********************************************************/

#include <periph/uart_zen.h>

/*! @defgroup defines Constants Definitions
* @{
*/

/*!\def RX_MASK
* Interrupt identification mask.
*/
#define RX_MASK 0x0E

/*!\def RX_INT
* Receive data available mask.
*/
#define RX_INT 0x04

/*!\def TX_EMPTY
* Transmitter holding register empty mask.
*/
#define TX_EMPTY 0x20

/*!\def UART_REG_OFFSET
* Makro for register address computation according respective UART number.
*/
#define UART_REG_ADDR(base_reg, dev_num) ( *( (volatile char *) ( ( (volatile void *) &base_reg ) + ( (dev_num) * UART1_OFFSET ) ) ) )
/*! @} */

volatile uint8_t err_flag[2] = {0, 0}; //!< UART0 and UART1 error flags
volatile uint16_t write_buffer_index[2] = {0, 0}, read_buffer_index[2] = {0, 0}; //!< UART0 and UART1 read and write buffer indexes
uint8_t buff[2][UART_BUFF_LEN]; //!< UART0 and UART1 data buffers

/*! UART0 interrupt handler prototype */
void UART0_irq ( void ) __attribute__ ( ( interrupt ) );

/*! UART1 interrupt handler prototype */
void UART1_irq ( void ) __attribute__ ( ( interrupt ) );

/*! UART interrupt service routine.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
*/
void UART_isr ( uint8_t uart_num ) {
	if ( !( err_flag[uart_num] & 0x1E ) ) err_flag[uart_num] |= UART_REG_ADDR ( U0LSR, uart_num ) & 0x1E; // check the line status
	
	if ( ( UART_REG_ADDR ( U0IIR, uart_num ) & RX_MASK ) == RX_INT ) { // if Rx data ready IRQ is pending
		buff[uart_num][write_buffer_index[uart_num]] = UART_REG_ADDR ( U0RBR, uart_num ); // store new character into the receive buffer
		if ( ++write_buffer_index[uart_num] >= UART_BUFF_LEN ) write_buffer_index[uart_num] = 0; // shift the write buffer index
		
		if ( write_buffer_index[uart_num] == read_buffer_index[uart_num] ) { // when round buffer is full (overflow condition)
			if ( write_buffer_index[uart_num] == 0 ) write_buffer_index[uart_num] = UART_BUFF_LEN - 1; else write_buffer_index[uart_num]--; // shift the write buffer index back
			UART_REG_ADDR ( U0IER, uart_num ) = 0x00; // disable Rx interrupt (to prevent new data reception)
			err_flag[uart_num] |= 0x01; // set the overflow condition bit in the error flag
		}
	}
}

/*! UART0 Rx interrupt service rutine.
* This rutine might be modified according user demands.
*/
void UART0_irq ( void ) {
	UART_isr ( UART0 ); // call UART0 ISR
	VICVectAddr = 0; // int acknowledge
}

/*! UART1 Rx interrupt service rutine.
* This rutine might be modified according user demands.
*/
void UART1_irq ( void ) {
	UART_isr ( UART1 ); // call UART1 ISR
	VICVectAddr = 0; // int acknowledge
}

/*! UART initialization function.
* Takes three arguments:
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \param baud_rate unsigned 32-bit int baud rate in bps
* \param pclk unsigned 32-bit int peripheral clock frequency in Hz
* \param rx_isr_vect unsigned 32-bit int interrupt vector number
*/
void UART_init ( uint8_t uart_num, uint32_t baud_rate, uint32_t pclk, unsigned rx_isr_vect ) {
	uint16_t divisor = ( pclk + ( ( baud_rate * 16 ) / 2 ) ) / ( baud_rate * 16 ); // baud divisor computation
	
	PINSEL0 &= ( 0xFFFFFFF0 - ( uart_num * 0xEFFF1 ) ); //enable UART functionality on respective processor pins
	PINSEL0 |= ( 0x00000005 + ( uart_num * 0x4FFFB ) );
	UART_REG_ADDR ( U0LCR, uart_num ) = 0x83; // 8-bit data, no parity, set DLAB = 1
	UART_REG_ADDR ( U0DLL, uart_num ) = *(uint8_t *) &divisor; // write divisor lo-byte
	UART_REG_ADDR ( U0DLM, uart_num ) = *( ( (uint8_t *) &divisor ) + 1 ); // write divisor hi-byte
	UART_REG_ADDR ( U0LCR, uart_num ) &= 0x7F; // clear DLAB
	UART_REG_ADDR ( U0FCR, uart_num ) = 0x01; // enable UART FIFO, interrupt level 1 byte - this might be modified by the user
	UART_REG_ADDR ( U0IER, uart_num ) = 0x01; // enable Rx interrupt
	
	if ( !uart_num ) ( ( uint32_t * ) &VICVectAddr0 )[rx_isr_vect] = ( uint32_t ) UART0_irq; // if UART0 register UART0 interrupt handler
	else ( ( uint32_t * ) &VICVectAddr0 )[rx_isr_vect] = ( uint32_t ) UART1_irq; // else register UART1 interrupt handler
	
	( ( uint32_t * ) &VICVectCntl0 )[rx_isr_vect] = 0x20 | ( 6 + uart_num ); // enable IRQ slot, set UART interrupt number
	VICIntEnable = ( ( uart_num + 1 ) * 0x00000040 ); //enable UART IRQ
}

/*! Data read function.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns readed character (unsigned 8-bit int).
*/
uint8_t read_UART_data ( uint8_t uart_num ) {
	uint8_t data; // readed data byte
	
	while ( read_buffer_index[uart_num] == write_buffer_index[uart_num] ); // wait for new Rx data
	
	data = buff[uart_num][read_buffer_index[uart_num]]; // read new data from the data buffer
	if ( ++read_buffer_index[uart_num] >= UART_BUFF_LEN ) read_buffer_index[uart_num] = 0; // shift the oldest unread byte index
	
	if ( ( err_flag[uart_num] & 0x01 ) && ( read_buffer_index[uart_num] == write_buffer_index[uart_num] ) ) { // if in overflow condition and buffer is empty
		err_flag[uart_num] &= 0xFE; // clear the overflow condition bit
		UART_REG_ADDR ( U0IER, uart_num ) = 0x01; // enable Rx interrupt (to allow new data reception)
	}
	
	return data; // return new data byte
}

/*! Determine possible UART transmission errors.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns error code (0 no errors; bit0 - incoming data buffer overrun, bit1 - overrun error, bit2 - parity error, bit3 - framing error bit4 - break interrupt).
*/
uint8_t UART_test_err ( uint8_t uart_num ) {
	uint8_t aux = err_flag[uart_num]; // store the error flag into auxiliary variable
	err_flag[uart_num] &= 0x01; // reset the error flag (but preserve the overflow condition bit)
	return aux; // return error code
}

/*! Determine whether new data wait in the UART round buffer.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns 1 if there are unreaded data in the incoming data buffer, 0 otherwise.
*/
uint8_t UART_new_data ( uint8_t uart_num ) {
	if ( read_buffer_index[uart_num] == write_buffer_index[uart_num] ) return 0; // check whether round buffer is empty
	else return 1; // if not empty, return 1
}

/*! Data write function.
* Takes two arguments:
* \param uart_num unsigned 8-bit UART number (0 or 1)
* \param data unsigned 8-bit int byte (character) to send
* \return Returns -1 if line was busy and no data were sent, 0 in case of success.
*/
int8_t write_UART_data ( uint8_t uart_num, uint8_t data ) {
	if ( !( UART_REG_ADDR ( U0LSR, uart_num ) & TX_EMPTY ) ) return -1; // if transmitter holding register is not empty, return -1
	UART_REG_ADDR ( U0THR, uart_num ) = data; // write the data byte to the transmitter holding register
	return 0; // return success
}
