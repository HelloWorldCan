#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <asm/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>


#define DEBUG 0

int cnt;
char * arg[10];

unsigned char * bbuf=NULL;
long Len=0;

int tohit(int argc, char **argv);

void SaveBB(int fd,char *buf,int len)
{
 char str[100];
 int i;
 int fd1;
 
 if (!bbuf){
  printf("Error bufer empty\n");
	return;
 }
 while(*buf && *buf!=' ') buf++;
 while(*buf && *buf==' ') buf++;
 i=0;
 while(*buf && *buf!=' ' && *buf!=10 && *buf!=13) str[i++]=*buf++;
 str[i]=0;
 
 if(i<1){
  printf("Error bad parametr \n");
	return;
 }
 if ((fd1 = open(str, O_CREAT | O_WRONLY, 0644)) == -1) {
  printf("Error openning %s\n",str);
	printf("%s\n",strerror(errno));
	return;
 }
 i=write(fd1,bbuf,Len);
 if(i<0){
	printf("%s\n",strerror(errno));
 }
 else if(i!=Len){
  printf("Error writing %s\n",str);
 }
 close(fd1);
 printf("Write %d bytes\n",i);
}

void SaveBA(int fd,char *buf,int len)
{
 char str[100];
 int i;
 FILE * F;
 __s16 * x;
 
 if (!bbuf){
  printf("Error bufer empty\n");
	return;
 }
 while(*buf && *buf!=' ') buf++;
 while(*buf && *buf==' ') buf++;
 i=0;
 while(*buf && *buf!=' ' && *buf!=10 && *buf!=13) str[i++]=*buf++;
 str[i]=0;
 
 if(i<1){
  printf("Error bad parametr \n");
	return;
 }
 F=fopen(str,"w");
 if (F==0) {
  printf("Error openning %s\n",str);
	printf("%s\n",strerror(errno));
	return;
 }
 x=(__s16 *)bbuf;
 for(i=0;i<(Len/2);i++){
  fprintf(F,"%07d\n",*x);
	x++;
 }
 fclose(F);
 printf("Write %d num\n",i);
}


void Load(int fd,char *buf,int len,int fl)
{
 int i;
 int j;
 char str[100];
 char a1[]="7";
 char * a[3];
 
 a[0]=NULL;
 a[1]=a1;
 a[2]=a1;
 
 buf[len]=' ';
 buf[len+1]=0;
 buf+=5;
 if(*buf!=' ') buf++;
 i=1;
 arg[0]=NULL;
 j=0;
 while(*buf){
  if(j>0 && *buf==' '){
	 str[j]=0;
	 j++;
	 if(i<cnt) free(arg[i]);
	 arg[i]=(char *)malloc(j);
	 do {
	  j--;
	  arg[i][j]=str[j];
	 }while(j);
	 i++;
	 if(i==10) i--;
	}
	else{
	 if(*buf!=' ' && *buf!=10 && *buf!=13) str[j++]=*buf;
	 if (j==100) j--;
	} 
	buf++;
 }
 if(i>1){
  cnt=i;
 }
 for (i=0;i<cnt;i++){
  printf("  A%d : %s\n",i,arg[i]);
 }
 if(cnt){
  if(fl==0){
	 write(fd,"LOAD\n",5);
	 usleep(100000);
	 read(fd,str,100);
	} 
	tohit(cnt,arg);
	tohit(3,a);
	usleep(200000);
	read(fd,str,100);
 }
 else{
  printf("Bad Parametr\n");
 }
}

char GName[100];
char GStart[20];
char GLen[20];
int GChan;
int GSt=0;
int Cb=0;

void Get(int fd,char *buf,int len)
{
 char str[100];
 int i;
 
 while(*buf && *buf!=' ') buf++;
 while(*buf && *buf==' ') buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) str[i++]=*buf++;
 str[i]=0;
 GChan=strtol(str,NULL,10);
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GStart[i++]=*buf++;
 GStart[i]=0;
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GLen[i++]=*buf++;
 GLen[i]=0;
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GName[i++]=*buf++;
 GName[i]=0;
 
 if(i<1){
  printf("Error bad parametr \n");
	return;
 }
 Cb=1;
 GSt=0;
 sprintf(str,"GET 0,%s,%s\n",GStart,GLen);
 printf("%s",str);
 write(fd,str,strlen(str));
}

void GetM(int fd,char *buf,int len)
{
 char str[100];
 int i;
 
 while(*buf && *buf!=' ') buf++;
 while(*buf && *buf==' ') buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) str[i++]=*buf++;
 str[i]=0;
 GChan=strtol(str,NULL,10);
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GStart[i++]=*buf++;
 GStart[i]=0;
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GLen[i++]=*buf++;
 GLen[i]=0;
 while(*buf && (*buf==' ' || *buf==',')) buf++;

 i=0;
 while(*buf && *buf!=',' && *buf!=' ' && *buf!=10 && *buf!=13) GName[i++]=*buf++;
 GName[i]=0;
 
 if(i<1){
  printf("Error bad parametr \n");
	return;
 }
 Cb=2;
 GSt=0;
 sprintf(str,"GET 0,%s,%s\n",GStart,GLen);
 printf("%s",str);
 write(fd,str,strlen(str));
}

void GetCB(int fd)
{
 char str[100];
 int i;
 FILE * F;
 __s16 * x;
 
 sprintf(str,"%s.%03d",GName,GSt);

 F=fopen(str,"w");
 if (F==0) {
  printf("Error openning %s\n",str);
	printf("%s\n",strerror(errno));
	return;
 }
 x=(__s16 *)bbuf;
 for(i=0;i<(Len/2);i++){
  fprintf(F,"%07d\n",*x);
	x++;
 }
 fclose(F);
 printf("Write %d num\n",i);
 GSt++;
 if(GSt>GChan-1){
  Cb=0;
	GSt=0;
	return;
 }
 sprintf(str,"GET %d,%s,%s\n",GSt,GStart,GLen);
 printf("%s",str);
 write(fd,str,strlen(str));
}

void GetMCB(int fd)
{
 char str[100];
 int i;
 FILE * F;
 __s16 * x;
 
 sprintf(str,"%s.dat",GName);
 if(GSt==0)F=fopen(str,"w");
 else F=fopen(str,"a");
 if (F==0) {
  printf("Error openning %s\n",str);
	printf("%s\n",strerror(errno));
	return;
 }
 x=(__s16 *)bbuf;
 for(i=0;i<(Len/2);i++){
  if(i==(Len/2)-1) fprintf(F,"%07d\n",*x);
  else fprintf(F,"%07d ",*x);
	x++;
 }
 fclose(F);
// printf("Write %d num\n",i);
 GSt++;
 if(GSt>GChan-1){
	printf("Write %d chanels\n",GSt);
  Cb=0;
	GSt=0;
	return;
 }
 sprintf(str,"GET %d,%s,%s\n",GSt,GStart,GLen);
 printf("%s",str);
 write(fd,str,strlen(str));
}

void LCmd(int fd,char* buf,int len)
{
 buf[len]=0;
 if((strstr(buf,"!loadb")==buf) || (strstr(buf,"!LOADB")==buf)) 
  Load(fd,buf,len,1);
 else if((strstr(buf,"!load")==buf) || (strstr(buf,"!LOAD")==buf)) 
  Load(fd,buf,len,0);
 else if((strstr(buf,"!savebuf")==buf) || (strstr(buf,"!SAVEBUF")==buf)) 
  SaveBB(fd,buf,len);
 else if((strstr(buf,"!saveasc")==buf) || (strstr(buf,"!SAVEASC")==buf)) 
  SaveBA(fd,buf,len);
 else if((strstr(buf,"!getm")==buf) || (strstr(buf,"!GETM")==buf)) 
  Get(fd,buf,len);
 else if((strstr(buf,"!get")==buf) || (strstr(buf,"!GET")==buf)) 
  GetM(fd,buf,len);
 else printf ("Unknown comand : %s\n",buf);
}

void CB(int fd)
{
 if(Cb==1) GetCB(fd);
 if(Cb==2) GetMCB(fd);
}


int main(int argc, char **argv) 
{
 int ii;
 int oi;
 int i;
 unsigned char ibuf[500];
 unsigned char obuf[500];
 unsigned char * sdev="/dev/ttyS1";
 int fd=-1;
 long len=0;
 long dlen=0;
 unsigned char *x;
 
 unsigned char * bb;
 
 cnt=0;
 
 if ((fd = open(sdev, O_RDWR | O_NONBLOCK)) == -1) {
  printf("Error openning %s\n",sdev);
	exit(-1);
 }	
 rs232_setmode(fd,38800,0,0);
 
 
 ii=0;
 oi=0;
 while(1){
  if(rs232_test(fd,10000)==1){
	 oi+=read(fd, obuf+oi, 500-oi);
	 if(obuf[oi-1]==0xa || obuf[oi-1]==0xd || oi>400){
	  obuf[oi]=0;
		if(dlen){
		 if(oi<=dlen){
			memcpy(bb,obuf,oi);
			bb+=oi;
		  dlen-=oi;
		  oi=0;
		 }
		 else{
			memcpy(bb,obuf,dlen);
			bb+=dlen;
		  x=obuf+dlen;
			dlen=0;
			i=0;
			while(x-obuf<oi){
		   obuf[i++]=*x++;
			}
			oi=i;
			obuf[oi]=0;
		 }
		 if(!dlen) CB(fd);
		}
		else {
	   x=strstr(obuf,"061:");
	   if(x){
			x+=4;
			sscanf(x,"%ld",&Len);
			printf("len=%ld\n",Len);
			len=Len;
			if(bbuf) free(bbuf);
			bbuf=(unsigned char *)malloc(len);
			bb=bbuf;
			while(*x!=0xa && *x!=0xd && *x) x++;
			if(*x==0xd || *x==0xa) x++;
			i=0;
			while(x-obuf<oi){
		   obuf[i++]=*x++;
			}
			oi=i;
			obuf[oi]=0;
		 }
	   x=strstr(obuf,"062:");
		 if(x){
		  dlen=len;
			len=0;
			x+=4;
			if(oi-(x-obuf)>=dlen){
			 memcpy(bb,x,dlen);
			 bb+=dlen;
		   x+=dlen;
			 dlen=0;
			 i=0;
			 while(x-obuf<oi){
		  	obuf[i++]=*x++;
			 }
			 oi=i;
			 obuf[oi]=0;
			}
			else{
			 memcpy(bb,x,oi-(x-obuf));
			 bb+=oi-(x-obuf);
		   dlen-=oi-(x-obuf);
		   oi=0;
			}
			if(!dlen) CB(fd);
		 }
		} 
		write(0, obuf, oi);
		oi=0;
	 }	
	}
  if(rs232_test(1,10000)==1){
	 ii+=read(1, ibuf+ii, 500-ii);
	 if(ibuf[ii-1]==0xa || ibuf[ii-1]==0xd || ii>400){
	  if(ibuf[0]=='!'){
		 LCmd(fd,ibuf,ii);
		}
		else{
		 write(fd, ibuf, ii);
		} 
		ii=0;
	 }	
	}
 }
}

