//
// C++ Interface: load_bfd
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

/**
 * load section callback function
 * -- called in loop for each section with LOAD flag, found in bfd file
 * @param lma load addres
 * @param size section length
 * @pram data data block to load
 * @return 0 if load was successful, non-0 to terminate caller loop
 */
typedef int (*load_section)(bfd_vma lma, bfd_size_type size, void *data);


int load_bfd(char *fname, char *target, load_section loader_cb,
             bfd_vma *start_address, int verbose);
