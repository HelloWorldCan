#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <ncurses.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include "tolpc_fn.h"

#define DEBUG 0
#define HAS_GETOPT_LONG 1

#define MEM_BUF_LEN 0x40000

unsigned char mem_buf[MEM_BUF_LEN];

int rs232_loop_test(char *sdev, int baud, int flowc);

struct termios init_saved_termios;


int flowc;

static void
usage(void)
{
  printf("usage: tohit <parameters> <send_file>\n");
  printf("  -d, --sdev <name>        name of RS232 device [%s]\n",tohit_sdev);
  printf("  -B, --baud <num>         RS232 baudrate [%d]\n",tohit_baud);
  printf("  -f, --flowc-rts          flow control\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc, char **argv) 
{
  /* FILE *F; */
  
  static struct option long_opts[] = {
    { "sdev",  1, 0, 'd' },
    { "baud",  1, 0, 'B' },
    { "flowc-rts",0, 0, 'f' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int opt;
  int ret;

  tohit_baud=9600;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:B:fVh")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:B:fVh",
			    &long_opts[0], NULL)) != EOF)
 #endif
  switch (opt) {
    case 'd':
      tohit_sdev=optarg;
      break;
    case 'B':
      tohit_baud = strtol(optarg,NULL,0);
      break;
    case 'f':
      flowc=1;
      break;
    case 'V':
      fputs("tohit pre alpha\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }

  def_shell_mode();
  savetty();
  /*tcgetattr(0, &init_saved_termios);*/
  initscr(); cbreak(); noecho();
  nonl(); intrflush(stdscr, FALSE); keypad(stdscr, TRUE);
  nodelay(stdscr, TRUE);
  
  ret=rs232_loop_test(tohit_sdev,tohit_baud,flowc);

  endwin();
  
  return ret;
}


int rs232_loop_test(char *sdev, int baud, int flowc)
{
  int fd;
  int c;
  unsigned char *pout=NULL, *pin=NULL, uc;
  int cntout=0,cntin=0, cnt;
  int i, test_loop_fl=0;
  int errorcnt=0;
  int idle;
  int stopin=0;
  /* int stopout=0; */
  
  /* Open RS232 device */
  if ((fd = open(sdev, O_RDWR | O_NONBLOCK)) == -1) {
    printf("Error openning %s\n",sdev);
    return -1;
  }	

  /* Set RS232 device mode and speed */
  if(rs232_setmode(fd,baud,0,flowc)<0){
    printf("Error in rs232_setmode\n");
    return -1;
  }

/*
  rs232_sendch(int fd,unsigned char c);
  rs232_recch(int fd);
  rs232_test(int fd,int time);
*/

  mvprintw(/*y*/2,/*x*/2,"Test of RS-232 transfers");

  do{
    c=getch();
    idle=(c==ERR);
    
    switch(c) {
      case 't' :
        cnt=20000;
        if(cnt>MEM_BUF_LEN) cnt=MEM_BUF_LEN;
        for(i=0;i<cnt;i++)
          mem_buf[i]=i^(i>>7);
        test_loop_fl=1;
	cntout=cntin=cnt;
	pout=pin=mem_buf;
        errorcnt=0;
        mvprintw(/*y*/11,/*x*/0,"Loop test : %s",
                 "Running");
        mvprintw(/*y*/9,/*x*/0,"                   ");
        mvprintw(/*y*/10,/*x*/0,"                   ");
        break;
     case 's' :
        stopin=!stopin;
        break;
    }
    
    if(test_loop_fl) {
      if(cntout)
        if(write(fd, pout, 1) == 1) {
          pout++;
          cntout--;
          idle=0;
        }
      if(cntin&&!stopin)
        if(read(fd, &uc, 1) == 1) {
          if(*pin!=uc) {
            errorcnt++;
            mvprintw(/*y*/9,/*x*/0,"Diff   : %02X != %02X",uc,*pin);
            mvprintw(/*y*/10,/*x*/0,"Errors : %4d",errorcnt);
          }
          pin++;
          cntin--;
          idle=0;
        }
      if(!cntin&&!cntout) {
        mvprintw(/*y*/11,/*x*/0,"Loop test : %s",
                 errorcnt?"Failed ":"Passed ");
        test_loop_fl=0;
      }
    }
    
    if(idle) {
      mvprintw(/*y*/8,/*x*/0,"Cnt Out: %6d In : %6d  %s",
      	       cntout,cntin,stopin?"Stop":"    ");
    }
  } while(c!=KEY_F(10));

  return 0;  
}
  
