#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdarg.h>
#include "tolpc_fn.h"
#include "load_bfd.h"

#define DEBUG 0
#define HAS_GETOPT_LONG 1


struct tolpc_env Tolpc =
{
    .sdev = "/dev/ttyS0", .crystal = 10000, .baud = 9600, .waitrep = 100000
};

struct tolpc_env *env = &Tolpc;

void error(const char *fmt, ...)
{
    va_list ap;
    char message[100];

    va_start(ap, fmt);
    vsnprintf(message, 100, fmt, ap);
    va_end(ap);

    fprintf(stderr, "tolpc error: %s\n", message);
    exit(1);
}

static void usage(void)
{
    printf("usage:\n");
    printf("  high level: tolpc [<global parameters>] -L [mode] -f <object file>\n");
    printf("   low level: tolpc [<global parameters>] [command [,...]]\n");
    printf("Command has the form: [command [, param1 [,param2, ...]]\n");
    printf("\n");
    printf("Global Parameters:\n");
    printf("  -d, --sdev <name>        name of RS232 device [%s]\n", env->sdev);
    printf("  -q, --crystal <kHz>      crystal frequency in kHz [%d]\n", env->crystal);
    printf("  -b, --baud <num>         RS232 baudrate [%d]\n", env->baud);
    printf("  -w, --wait <num>         timeout in miliseconds [%ld]\n", env->waitrep/1000);
    printf("  -v, --verbose            increase verbosity (can be used more times)\n");
    printf("Commands:\n");
    printf("  -L, --load [A|T]         Load binary file and execute (loads all BFD sections,\n");
    printf("                           jumps to start address) [only RAM in this version].\n");
    printf("  -U, --unlock\n");
    printf("  -A, --echo <setting>     Echo (use -A0 when uploading data using W)\n");
    printf("  -G, --go [<mode>]        Mode is A or T [A]\n");
    printf("  -C, --copy <flash_addr>  Copy RAM to Flash\n");
    printf("  -c, --cmd <command>      Execute other command (letters from LPC user manual)\n");
    printf("Parameters:\n");
    printf("  -s, --start  <addr>      start address of transfer\n");
    printf("  -l, --length <num>       length of upload block\n");
    printf("  -f, --file <filename>    file to read or write\n");
    printf("  -o, --offset <num>       offset in a file\n");
    printf("Other:\n");
    printf("  -k, --break              send communication break character\n");
    printf("  -V, --version            show version\n");
    printf("  -h, --help               this usage screen\n");
}

struct cmd_params {
    char cmd;
    char *sarg;
    int iarg;
    
    long start;
    int length;
    int offset;
    char *fname;

    void *data;
};

int load_file(struct cmd_params *p)
{
    FILE *f;
    struct stat st;
    int ret;

    if (!p->fname)
        error("No file (-f) for %c command", p->cmd);

    ret = stat(p->fname, &st);
    if (ret) {
        perror(p->fname);
        exit(1);
    }

    if (p->length == 0) p->length = st.st_size - p->offset;

    f = fopen(p->fname, "r");
    if (f == NULL) {
        perror(p->fname);
        exit(1);
    }

    fseek(f, p->offset, SEEK_SET);

    p->data = malloc(p->length);

    fread(p->data, p->length, 1, f);
    if (ferror(f)) {
        perror(p->fname);
        exit(1);
    }
    
    return p->length;
}

int ram_write_cb(bfd_vma lma, bfd_size_type size, void *data) {
    return tolpc_write_ram(env, lma, size, data);
}

/**
 * load bfd file to target (only RAM at this moment) and jump
 * to its start address
 * @return 0=OK
 */
int load_exec(struct cmd_params *p) {
    int ret;
    bfd_vma start;

    if (!p->fname)
        error("No file (-f) for %c command", p->cmd);

    ret = tolpc_echo(env, '0');
    if (ret) return ret;
    
    ret = tolpc_unlock(env);
    if (ret) return ret;
    
    ret = load_bfd(p->fname, /*default target*/NULL, ram_write_cb, &start, tolpc_verbose_level);
    if (ret) return ret;

    tolpc_verbose(1, "Running the program\n");
    ret = tolpc_go(env, start,  p->iarg == 'A');

    return(ret);
}

void not_implemented(char c)
{
    error("Not implemented: %c", c);
}

void clear_cmd(struct cmd_params *p)
{
    if (p->data) free(p->data);
    memset(p, 0, sizeof(*p));
    p->start = -1;
}

void run_cmd(struct cmd_params *p)
{
    int ret = 0;
    
    switch (p->cmd) {
        case 'U':
            ret = tolpc_unlock(env);
            break;
        case 'A':
            ret = tolpc_echo(env, p->iarg);
            break;
        case 'G':
            if (p->iarg != 'A' && p->iarg != 'T')
                error("Go mode can only be A or T.");
            if (p->start < 0)
                error("No start address is given for Go command");
            ret = tolpc_go(env, p->start, p->iarg == 'A');
            break;
        case 'W':
            load_file(p);
            if (p->start < 0)
                error("No start addres is given for Write command");
            ret = tolpc_write_ram(env, p->start, p->length, p->data);
            break;
        case 'J':
        {
            char partid[100];
            ret = tolpc_partid(env, partid, 99);
            if (ret == 0) {
                tolpc_verbose(1, "Part ID:");
                puts(partid);
            }
            break;
        }
        case 'K':
        {
            char ver[100];
            tolpc_bootver(env, ver, 99);
            if (ret == 0) {
                tolpc_verbose(1, "Boot code version:");
                puts(ver);
            }
            break;
        }
        case 'L':
	    ret = load_exec(p);
	    break;
        default:
            not_implemented(p->cmd);

    }
    if (ret) error("Command %c", p->cmd);
}

void new_command(struct cmd_params *p, char cmd)
{
    if (p->cmd)
        run_cmd(p);
    
    clear_cmd(p);
    p->cmd = cmd;        
}

int main(int argc, char **argv)
{
    int i;
    int opt;
    char short_ops[100];
    struct cmd_params p;

    static struct option long_opts[] = {
        { "sdev", 1, 0, 'd'},
        { "crystal", 1, 0, 'q' },
        { "baud", 1, 0, 'b' },
        { "wait", 1, 0, 'w' },
        { "verbose", 0, 0, 'v' },

        { "unlock", 0, 0, 'U' },
        { "echo", 1, 0, 'A' },
        { "go", 2, 0, 'G' },
        { "copy", 1, 0, 'C' },
        { "cmd", 1, 0, 'c' },
        { "load", 2, 0, 'L' }, // has optional argument

        { "start", 1, 0, 's' },
        { "length", 1, 0, 'l' },
        { "file", 1, 0, 'f' },
        { "offset", 1, 0, 'o' },

        { "break", 0, 0, 'k' },
        { "version", 0, 0, 'V' },
        { "help", 0, 0, 'h' },
        { 0, 0, 0, 0}
    };

    // Initial clear of of parameters structure (clear_cmd produces segfault here)
    memset(&p, 0, sizeof(p));
    p.start = -1;

    memset(short_ops, 0, sizeof(short_ops));
    i = 0;
    for (opt = 0; long_opts[opt].name != NULL || i > 100 - 3; opt++) {
        short_ops[i++] = long_opts[opt].val;
        if (long_opts[opt].has_arg) short_ops[i++] = ':';
        if (long_opts[opt].has_arg == 2) short_ops[i++] = ':';
    }
    short_ops[i] = '\0';

#ifndef HAS_GETOPT_LONG
    while ((opt = getopt(argc, argv, short_ops)) != EOF)
#else
        while ((opt = getopt_long(argc, argv, short_ops,
                                  &long_opts[0], NULL)) != EOF)
#endif
            switch (opt) {
            case 'd':
                env->sdev = optarg;
                break;
            case 'b':
                env->baud = strtol(optarg, NULL, 0);
                break;
            case 'q':
                env->crystal = strtol(optarg, NULL, 0);
                break;
            case 'w':
                env->waitrep = 1000*strtol(optarg, NULL, 0);
                break;
            case 'v':
                tolpc_verbose_level++;
                break;
            case 'U':
                new_command(&p, opt);
                break;
            case 'A':
                new_command(&p, opt);
                p.iarg = optarg[0];
                break;
            case 'G':
            {
                new_command(&p, opt);
                p.iarg = 'A'; // ARM mode is default
                if (optarg) p.iarg = optarg[0];
                break;
            }
            case 'C':
                new_command(&p, opt);
                break;
            case 'c':
            {
                char cmd = optarg[0];
                switch (cmd) {
                    case 'W':
                    case 'J':
                    case 'K':
                        new_command(&p, cmd);
                        break;
                    default:
                        error("Unknown command %c", cmd);
                        break;
                }
                break;
            }
	    case 'L':
                new_command(&p, opt);
                p.iarg = 'A'; // ARM mode is default
                if (optarg) p.iarg = optarg[0];
            break;
            case 's':
                p.start = strtol(optarg, NULL, 0);
                break;
            case 'l':
                p.length = strtol(optarg, NULL, 0);
                break;
            case 'f':
                p.fname = optarg;
                break;
            case 'o':
                p.offset = strtol(optarg, NULL, 0);
                break;
            case 'V':
                fputs("tolpc pre alpha\n", stdout);
                exit(0);
            case 'h':
            default:
                usage();
                exit(opt == 'h' ? 0 : 1);
            }
            new_command(&p, 0); // run the last command
    return 0;
}
