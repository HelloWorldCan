#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include "tolpc_fn.h"
#include "uuencode.h"

#define DEBUG 0
#define DEBUG_COUNT 0

//#define WITHOUT_CFSETSPEED

int tolpc_debug_level = 0;

#ifdef WITHOUT_CFSETSPEED

#include "baudrate.h"

/* Set both the input and output baud rates stored in *TERMIOS_P to SPEED.  */
int rs232_cfsetspeed (struct termios *termios_p, speed_t speed) {
  size_t cnt;
  
  for (cnt = 0; cnt < sizeof(rs232_speeds) / sizeof(rs232_speeds[ 0 ]); ++cnt)
    if (speed == rs232_speeds[ cnt ].internal) {
      cfsetispeed (termios_p, speed);
      cfsetospeed (termios_p, speed);
      return 0;
    } else if (speed == rs232_speeds[ cnt ].value) {
      cfsetispeed (termios_p, rs232_speeds[ cnt ].internal);
      cfsetospeed (termios_p, rs232_speeds[ cnt ].internal);
      return 0;
    }
  
  return -1;
}

#endif /* WITHOUT_CFSETSPEED */

#define FLOWC_XONXOFF 0x02
#define FLOWC_RTSCTS  0x01

int tolpc_verbose_level = 0;

void tolpc_verbose(int level, const char *fmt, ...)
{
    va_list ap;

    if (level <= tolpc_verbose_level) {
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        fflush(stdout);
    }
}
#define verbose tolpc_verbose

/**
 * Set right mode and speed for RS232 interface
 * baud can be either speed in character per second or special Bxxxx constant
 */
int setup_comm_dev(int dev, int baudrate, int flowc) {
  struct termios term_attr;

  if (tcgetattr(dev, &term_attr)) {
    perror("setup_dev: tcgetattr");
    return -1;
  }
  /* set immediate character input */
  /*
    ~ICANON -- do not wait for EOL at input,
    ~ECHO -- do not echo characters locally
  */
  term_attr.c_lflag = 0;
  term_attr.c_cc[VMIN] = 1;
  term_attr.c_cc[VTIME] = 0;

  /* input newlines: \r\n=>\n */
  term_attr.c_iflag = IGNCR;
  if (flowc & FLOWC_XONXOFF)
    term_attr.c_iflag |= IXON | IXOFF;
  if (flowc & FLOWC_RTSCTS)
    term_attr.c_cflag |= CRTSCTS;
  
  term_attr.c_oflag = 0;

  /* set right ispeed and ospeed */
  #ifdef WITHOUT_CFSETSPEED
  if (rs232_cfsetspeed(&term_attr, baudrate) < 0) {
    fprintf(stderr, "Error in rs232_cfsetspeed\n");
    return -1;
  }
  #else /* WITHOUT_CFSETSPEED */
  if (cfsetspeed(&term_attr, baudrate) < 0) {
    fprintf(stderr, "Error in cfsetspeed\n");
    return -1;
  }
  #endif /* WITHOUT_CFSETSPEED */

  if (tcsetattr(dev, TCSAFLUSH, &term_attr)) {
    perror("setup_dev: tcsetattr");
    return -1;
  }

  return 0;
}

/* * * * */

static const char *isp_response_code[] = {
  "CMD_SUCCESS",
  "INVALID_COMMAND",
  "SRC_ADDR_ERROR: Source address is not on word boundary",
  "DST_ADDR_ERROR: Destination address is not on a correct boundary",
  "SRC_ADDR_NOT_MAPPED: Source address is not mapped in the memory map",
  "DST_ADDR_NOT_MAPPED: Destination address is not mapped in the memory map",
  "COUNT_ERROR: Byte count is not multiple of 4 or is not a permitted value",
  "INVALID_SECTOR: Sector number is invalid or end sector number is "
  " greater than start sector number.",
  "SECTOR_NOT_BLANK",
  "SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION:"
  " Command to prepare sector for write operation was not executed",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
};

typedef enum _response_type_t {
  NO_RESPONSE,
  NUM_RESPONSE,
  OK_RESPONSE
} resp_type_t;

int poll_input(FILE *stream, long tv_sec, long tv_usec) {
  fd_set sel_set;
  struct timeval timeout;
  int sel, comm_dev = fileno(stream);

  FD_ZERO(&sel_set);
  FD_SET(comm_dev, &sel_set);
  timeout.tv_sec = tv_sec;
  timeout.tv_usec = tv_usec;
  sel = select(comm_dev+1, &sel_set, NULL, NULL, &timeout);
  if (sel == -1)
    perror("poll_input: select");
  return(sel);
}

int send_line(FILE *comm, resp_type_t response_type, char *line) {
  char s[160], *p;
  int sel, resp;
  size_t size;

  /* send command */
  /*
    according to documentation, \n should be sufficient
    -- works well with commands, but uuencoded data transmission
    seems to not work well with it; so using \r\n instead
  */
  if (fprintf(comm, "%s\r\n", line) < 0) {
    perror("send_line: fprintf");
    return -1;
  }

  /* check echo, 1s timeout */
  if ((sel = poll_input(comm, 1, 0)) < 1) {
    if (sel == 0)
      fprintf(stderr, "send_line: command echo timeout\n");
    else
      perror("send_line: select");
    return(1);
  }
  fgets(s, sizeof(s), comm);
  /*DEBUG*/printf(":%s", s);
  if (strncmp(line, s, strlen(line))) {
    fprintf(stderr, "send_line: \"%s\" command echo mismatch (\"%s\" received)\n", line, s);
    return(1);
  }

  if (response_type == NO_RESPONSE)
    return 0;

  p=s;
  /* read with timout for every character (necessary for go command) */
  do {
    /* check response, 1s timeout */
    if ((sel = poll_input(comm, 1, 0)) < 1) {
      *p = '\0';
      /*DEBUG*/printf(">%s", s);
      fflush(stdout);
      if (sel == 0) {
	fprintf(stderr, "send_line: \"%s\" command response timeout\n", line);
      }
      else
	perror("send_line: select");
      return(1);
    }
    /* read one character */
    size = fread(p, 1, 1, comm);
    p++;
  } while (*(p-1) != '\n');

  *p = '\0';
  /*DEBUG*/printf(">%s", s);

  if (response_type == OK_RESPONSE) {
    /* textual response -- expecting "OK" */
    resp = (strncmp(s, "OK", 2) != 0);
    if (resp)
      fprintf(stderr, "Received something else than \"OK\"\n");
    return(resp);
  }
  else {
    /* numeric response */
    if (sscanf(s, "%d", &resp) == 0) {
      fprintf(stderr, "Invalid (non-numeric) command response received\n");
      return(-1);
    }
    if (resp)
      fprintf(stderr, "Error response code %d\n", resp);
    return(resp);
  }
}

int send_line_fmt(FILE *comm, resp_type_t resp, const char *fmt, ...) {
  char s[160];
  va_list ap;

  va_start(ap, fmt);
  vsprintf(s, fmt, ap);
  va_end(ap);
  return send_line(comm, resp, s);
}

#define MAX_SYNC_QM_TRIES 240
int synchronize(FILE *comm, int crystal_freq) {
  int n, sel;
  char ch = '\0';
  char sync_string[] = "Synchronized\n";
  int sync_status = 0, sync_pos = 0, sync_len = strlen(sync_string);

  printf("Synchronizing");
  /* send '?'s and wait for response */
  for (n = 0; n < MAX_SYNC_QM_TRIES; n++) {
    /* if not receiving "Synchronized" response, send '?'s */
    if (!sync_status) {
      fputc('?', comm);
      fflush(comm);
    }

    /* poll for input */
    /* 0.1s is duration of 3 characters in 8N1 300b/s transfer */
    sel = poll_input(comm, 0, 100000);
    if (sel == -1) {
      perror("synchronize: select");
      return(1);
    }

    if (sel > 0) {
      ch = fgetc(comm);
      if (ch == sync_string[sync_pos]) {
	if (sync_pos == 0)
	  sync_status = 1;
	sync_pos++;
	if (sync_pos == sync_len) {
	  printf("\n\"Synchronized\" received (recv. %d chars)\n", n);
	  break;
	}
      }
      else {
	sync_status = 0;
	sync_pos = 0;
      }
    }
    /* show progress */
    if (!sync_status) {
      putchar(sel ? ((ch == '?') ? '.' : 'x') : '_');
      fflush(stdout);
    }
  }
  if (n == MAX_SYNC_QM_TRIES) {
    fprintf(stderr, "Didn't synchronize after %d tries.\n", n);
    return(1);
  }
  /*
   * here may be comm.dev mode switched back to ICANON...
   */
  /* respond to sync */
  if (send_line(comm, OK_RESPONSE, "Synchronized"))
    return(1);
  printf("Synchronized, OK.\n");
  /* send Xtal frequency */
  printf("Sending crystal frequency: %d\n", crystal_freq);
  //sprintf(s, "%d", crystal_freq);
  if (send_line_fmt(comm, OK_RESPONSE, "%d", 7373)) //crystal_freq))
    return 1;

  return 0;
}

struct termios comm_attr_save;

void close_comm(FILE *comm) {
  /* restore serial line settings */
  tcsetattr(fileno(comm), TCSANOW, &comm_attr_save);
  fclose(comm);
}

FILE *open_comm(char *device, int baudrate, int flowc) {
  FILE *comm;
  int comm_dev, line;

  /* TODO: Open the serial line the same way as lpc21isp. It seems
   * that loading works even if there is terminal emulator attached to
   * the same port. */
  if ((comm = fopen(device, "r+")) == NULL) {
    perror("open_comm: fopen");
    return(NULL);
  }
  /* set stream as unbuffered */
  setbuf(comm, NULL);

  comm_dev = fileno(comm);
  /* backup serial line settings */
  if (tcgetattr(comm_dev, &comm_attr_save))
    fprintf(stderr, "Warning: open_comm: can not get serial line settings by tcgetattr");
  /* setup serial device parameters */
  setup_comm_dev(comm_dev, baudrate, flowc);

  /* initialize serial bootloader (on miniARM_DB board) */
  ioctl (comm_dev, TIOCMGET, &line);
  line |= TIOCM_DTR;
  ioctl (comm_dev, TIOCMSET, &line);
  usleep(1000*1000);
  line &= ~TIOCM_DTR;
  ioctl (comm_dev, TIOCMSET, &line);
  usleep(100*1000);

  return(comm);
}

/* * * * */

int write_ram(FILE *comm, unsigned start, unsigned length, char *data) {
  char s[100];
  int i, line, line_len;
  unsigned count = length;
  unsigned checksum;

  sprintf(s, "W %u %u", start, length);
  if (send_line(comm, NUM_RESPONSE, s))
    return(1);
  while (count > 0) {
    checksum = 0;
    for (line = 0; (line < 20) && (count > 0); line++) {
      line_len = (count < 45) ? count : 45;
      for (i = 0; i < line_len; i++)
	checksum += (unsigned char)data[i];
      uuencode_line(s, data, line_len);
      count -= line_len;
      data += line_len;
      if (send_line(comm, NO_RESPONSE, s))
	return(1);
    }
    sprintf(s, "%u", checksum);
    if (send_line(comm, OK_RESPONSE, s))
      return(1);
  }
  return 0;
}

/* * * * */

int tolpc_send_cmd(struct tolpc_env *env, char *fmt, ...) {
  va_list ap;
  char s[160];

  if (env->comm_stream == NULL)
    if (tolpc_open_target(env))
      return -1;

  va_start(ap, fmt);
  vsprintf(s, fmt, ap);
  va_end(ap);

  return send_line(env->comm_stream, NUM_RESPONSE, s);
}

int tolpc_readline_to(struct tolpc_env *env, char *s, int size) {
  fgets(s, size, env->comm_stream);
  return 0;
}

int tolpc_open_target(struct tolpc_env *env) {
  FILE *comm;

  comm = open_comm(env->sdev, env->baud, FLOWC_XONXOFF);
  if (comm == NULL)
    return -1;
  if (synchronize(comm, env->crystal))
    return -1;

  env->comm_stream = comm;
  return 0;
}

int tolpc_write_ram(struct tolpc_env *env, unsigned int start, int length, unsigned char *data) {
  return write_ram(env->comm_stream, start, length, data);
}

/* * */

int tolpc_unlock(struct tolpc_env *env) {
  int ret;
  ret = tolpc_send_cmd(env, "U 23130");
  return ret;
}

int tolpc_go(struct tolpc_env *env, unsigned int start, char arm_mode)
{
    int ret;

    ret = tolpc_send_cmd(env, "G %u %c", start, arm_mode ? 'A' : 'T');
    if (ret == 2)
    {
        verbose(1, "Timeout means the code is executed\n");
        ret = 0;
    }
    return ret;
}

/**
 * Reads part ID
 * @param env
 * @param part_id Where to store received part id.
 * @param size Size of part_id.
 * @return Zero on success, -1 on error.
 */
int tolpc_partid(struct tolpc_env *env, char* part_id, int size)
{
    int ret;

    ret = tolpc_send_cmd(env, "J");
    if (ret) return ret;

    ret = tolpc_readline_to(env, part_id, size);
    return ret == 0 ? 0 : -1;
}

/**
 * Reads boot code version
 * @param env
 * @param ver Where to store received version.
 * @param size Size of ver.
 * @return Zero on success, -1 on error.
 */
int tolpc_bootver(struct tolpc_env *env, char* ver, int size)
{
    int ret;

    ret = tolpc_send_cmd(env, "K");
    if (ret) return ret;

    ret = tolpc_readline_to(env, ver, size);
    return ret == 0 ? 0 : -1;
}

/**
 * Sets echo mode
 * @param env
 * @return Zero on success, 1 on bad response, -1 on communication error.
 */
int tolpc_echo(struct tolpc_env *env, char on)
{
#if 0
    int ret;
    ret = tolpc_send_cmd(env, "A %c", on);
    return ret;
#else
    return 0;
#endif
}

int tolpc_break(struct tolpc_env *env)
{
#if 0
    int fd;
    int i;
    char c = 0;

    /* Open RS232 device */
    if ((fd = open(env->sdev, O_RDWR | O_NONBLOCK)) == -1) {
        printf("Error openning %s\n", env->sdev);
        return -1;
    }

    if (rs232_setmode(fd, env->baud / 2, 0) < 0) {
        close(fd);
        return -1;
    }

    #if DEBUG
    printf("Sending break chars \n");
    #endif

    for (i = 100;i--;)
        write(fd, &c, 1);

    close(fd);
#endif
    return 1;
}
