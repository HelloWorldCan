#ifndef _TOLPC_FN_H
#define _TOLPC_FN_H

#include <bfd.h>

extern int tolpc_verbose_level;

struct tolpc_env {
    // File descriptor and stream of open serial line
    int fd;
    //FILE *f;

    char *sdev;                 // Serial device to open
    int baud;                   // Communication baudrate
    int crystal;                // Crtystal freq of LPC21xx
    long waitrep;               // How long to wait for reply.
  FILE *comm_stream;
};

void tolpc_verbose(int level, const char *fmt, ...);
//int tolpc_synchronize(struct tolpc_env *env);

int tolpc_open_target(struct tolpc_env *env);
int tolpc_go(struct tolpc_env *env, unsigned int start, char arm_mode);
int tolpc_partid(struct tolpc_env *env, char* part_id, int size);
int tolpc_break(struct tolpc_env *env);
int tolpc_write_ram(struct tolpc_env *env, unsigned int start, int length, unsigned char *data);
int tolpc_unlock(struct tolpc_env *env);
int tolpc_bootver(struct tolpc_env *env, char* part_id, int size);
int tolpc_echo(struct tolpc_env *env, char on);

#endif /* _TOLPC_FN_H */


