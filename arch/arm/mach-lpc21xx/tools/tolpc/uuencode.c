/*
*  C Implementation: uuencode
*
* Description: 
*
*
* Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2005
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <inttypes.h>
#include "uuencode.h"

static char uuencode_table[64];
static char initialized = 0;
/**
 * Initializes uuencode_table.
 */
void uuencode_init() {
    int i;
    uuencode_table[0] = 0x60;

    for(i = 1; i < 64; i++) {
        uuencode_table[i] = (char)(0x20 + i);
    }
    initialized = 1;
}

/**
 * Uuencodes up to 45 bytes of data. The output string is not terminated by any newline character.
 * @param dest Where to store output string. There should be space at least for 62 bytes.
 * @param source 
 * @param length 
 * @return Zero on success, -1 on error.
 */
int uuencode_line(char *dest, unsigned char *source, int length) {
    int i;
    int data = 0;

    if (!initialized)
        uuencode_init();

    if (length > 45 || length < 0)
        return -1;

    *dest++ = uuencode_table[length];
    for (i = 0; i < length; i+=3) {
        data = *source++;
        data = (data << 8) | ((i+1 < length) ? *source++ : 0);
        data = (data << 8) | ((i+2 < length) ? *source++ : 0);

        *dest++ = uuencode_table[(data >> 18) & 0x3f];
        *dest++ = uuencode_table[(data >> 12) & 0x3f];
        *dest++ = uuencode_table[(data >>  6) & 0x3f];
        *dest++ = uuencode_table[ data        & 0x3f];
    }
    *dest = '\0';
    return 0;
}
