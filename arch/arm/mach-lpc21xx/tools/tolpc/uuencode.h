//
// C++ Interface: uuencode
//
// Description: 
//
//
// Author: Michal Sojka <sojkam1@fel.cvut.cz>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _UUENCODE_H
#define _UUENCODE_H

int uuencode_line(char *dest, unsigned char *source, int length);

#endif
