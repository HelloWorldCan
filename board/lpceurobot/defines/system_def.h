/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_def.h - common cover for definition of hardware adresses,
                 registers, timing and other hardware dependant
		 parts of embedded hardware
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <types.h>

#define WITH_SFI_SEL

#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
/* Software version */
#define SW_VER_ID	"LPCEUROBOT"
#define SW_VER_MAJOR	0
#define SW_VER_MINOR	1
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"LPCEUROBOT"
#define HW_VER_MAJOR	1
#define HW_VER_MINOR	0
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID	"LPCEUROBOT"
#define MECH_VER_MAJOR  0
#define MECH_VER_MINOR  0
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)

#define BOARD_LPCEUROBOT

#define CPU_REF_HZ 14745000l 	 /* reference clock */
#define CPU_SYS_HZ (4*CPU_REF_HZ)/* system clock */
#define CPU_APB_HZ (CPU_SYS_HZ/2)/* APB clock */
#define CPU_VPB_HZ CPU_APB_HZ	 /* VPB clock = APB clock, multiple definition */

unsigned long cpu_ref_hz;	/* actual external XTAL reference */
unsigned long cpu_sys_hz;	/* actual system clock frequency  */

volatile unsigned long msec_time;

#define SCI_RS232_CHAN_DEFAULT 1

/* #define DEB_LED_INIT() \ */
/* 	do {\ */
/* 	*DIO_PEDR=0x00;\ */
/*   	SHADOW_REG_SET(DIO_PEDDR,0x0f); /\* set PJ.1, PJ.2, PJ.3 LED output *\/ \ */
/* 	} while (0) */
	
/* #define DEB_LED_OFF(num) \ */
/*     (*DIO_PEDR |= PEDR_PE0DRm << (num)) */
/* #define DEB_LED_ON(num) \ */
/*     (*DIO_PEDR &=~(PEDR_PE0DRm << (num))) */


#endif /* _SYSTEM_DEF_H_ */
