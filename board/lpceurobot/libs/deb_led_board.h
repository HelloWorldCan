#ifndef DEB_LED_BOARD_H
#define DEB_LED_BOARD_H

#include <lpc21xx.h>

#define __LED_SHIFT 21
#define __LED_MASK 0x0f

#define LEDR (1<<0)		// LED R (1<<21)
#define LEDG (1<<1)		// LED G (1<<22)
#define LEDB (1<<2)		// LED B (1<<23)
#define LEDY (1<<3)		// LED Y (1<<24)

#define DEB_LED_ERROR LEDR	/* Error occured */
#define DEB_LED_RUN   LEDG	/* Should blink when running */

static inline unsigned
__deb_led_get()
{
	return (IOPIN0 >> __LED_SHIFT) & __LED_MASK;
}

static inline void
__deb_led_on(unsigned leds)
{
	IOSET0 = (leds & __LED_MASK) << __LED_SHIFT;
}

static inline void
__deb_led_off(unsigned leds)
{
	IOCLR0 = (leds & __LED_MASK) << __LED_SHIFT;
}

static inline void
__deb_led_set(unsigned leds)
{
	__deb_led_on(leds);
	__deb_led_off(~leds);
}

static inline void
__deb_led_change(unsigned leds)
{
	__deb_led_set(__deb_led_get() ^ leds);
}

static inline void
__deb_led_init()
{
	IO0DIR |= (__LED_MASK << __LED_SHIFT);
	__deb_led_set(0);
}

#endif
