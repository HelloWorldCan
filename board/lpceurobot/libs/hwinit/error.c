#include <deb_led.h>
#include <error.h>

static void
waitblink(int len)
{
	while (len--) {
		unsigned int i =2000000;
		while(--i);
	}
}

void error(enum error err)
{
	if (err == SUCCESS)
		return;
	/* For case of unintetional rewrite of IO port setting. */
	deb_led_init();
	while (1) {
		int i;
		for (i=7; i>=0; i--) {
			deb_led_set(~0);
			if (err & (1<<i))
				waitblink(3); /* 1 */
			else
				waitblink(1); /* 0 */
			deb_led_set(DEB_LED_ERROR);
			waitblink(1);
				
		}
		waitblink(5);		
	}
}
