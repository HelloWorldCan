#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <deb_led.h>
#include "startcfg.h"
#include <stdlib.h>
#include <error.h>

/* Called automatically from crt0.S before main() */
/* void __hardware_init(void) __attribute__ ((used)); */
void __hardware_init(void)
{
	int err = SUCCESS;

	if ((void*)&__hardware_init > (void*)0x40000000) {
		/* We are running from RAM */
		MEMMAP = 0x2;	/* Remap interrupt vectors */
	}

	deb_led_init();
	
	err = init_PLL(PLL_MUL_4 ,PLL_DIV_2 ,PLL_MODE_ENABLE);   //58.98MHz
	if (err) error(err);
					  
	err = init_MAM(MAM_FULL);   //58.98MHz
	if (err) error(err);

	set_APB(APB_DIV_2);
	

}

/* Put a pointer to this function in .init_array section */
void (*fp) (void) __attribute__ ((section (".init_array"))) = __hardware_init;
